package com.telkomsel.nami;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.telkomsel.nami.universal.PlaceJadwalActivity;
import com.telkomsel.nami.universal.PlaceListActivity;
import com.telkomsel.nami.universal.PlaceProfileActivity;
import com.telkomsel.nami.universal.core.Place;
import com.telkomsel.nami.universal.core.StaticVariable;
import com.telkomsel.nami.universal.core.User;

import java.util.Timer;
import java.util.TimerTask;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        TextView tvPhone = (TextView) findViewById(R.id.tvHomePhone);
        TextView tvHomeGreeting = (TextView) findViewById(R.id.tvHomeGreeting);

        tvPhone.setText(User.instance.getUsername() + " (" + User.instance.getPhoneOrUserId() + ")");
        tvHomeGreeting.setText("Sebagai " + User.instance.getUserType() + " " + User.instance.getBranch());

        final Button buttonOssOsk = (Button) findViewById(R.id.button_home_ossosk);
        final Button buttonSekolah = (Button) findViewById(R.id.button_home_sekolah);
        final Button buttonEvent = (Button) findViewById(R.id.button_home_event);
        final Button buttonFeedbackSekolah = (Button) findViewById(R.id.button_home_feedback_sekolah);
        final Button buttonFeedbackOssOsk = (Button) findViewById(R.id.button_home_feedback_ossosk);
        final Button buttonSummary = (Button) findViewById(R.id.button_home_summary);

        View.OnClickListener onButtonClicked = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == buttonOssOsk.getId()){
                    buttonPlaceClicked(Place.PlaceType.OSS_OSK);
                }else if(view.getId() == buttonSekolah.getId()){
                    buttonPlaceClicked(Place.PlaceType.SEKOLAH);
                }else if(view.getId() == buttonEvent.getId()){
                    buttonPlaceClicked(Place.PlaceType.EVENT);
                }else{
                    Toast.makeText(HomeActivity.this, "Maaf, fitur ini masih dalam tahap pengembangan", Toast.LENGTH_SHORT).show();
                }
            }
        };

        buttonOssOsk.setOnClickListener(onButtonClicked);
        buttonSekolah.setOnClickListener(onButtonClicked);
        buttonEvent.setOnClickListener(onButtonClicked);
        buttonFeedbackSekolah.setOnClickListener(onButtonClicked);
        buttonFeedbackOssOsk.setOnClickListener(onButtonClicked);
        buttonSummary.setOnClickListener(onButtonClicked);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_signout){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Apakah Anda yakin logout dari Nami?").setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                SharedPreferences globalPreferences = getSharedPreferences("global_preferences", Context.MODE_PRIVATE);
                globalPreferences.edit().remove("last_login_phone").remove("last_login_password").commit();
                finish();
                }
            }).setNegativeButton("Tidak", null).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    private boolean isExitOnBack = false;

    @Override
    public void onBackPressed() {
        if(isExitOnBack){
            finish();
        } else {
            Toast.makeText(this, "Tekan back sekali lagi untuk keluar", Toast.LENGTH_SHORT).show();
            isExitOnBack = true;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    isExitOnBack = false;
                }
            }, 2000);
        }
    }

    private void buttonPlaceClicked(final Place.PlaceType placeType){
        final ProgressDialog progressDialog = new ProgressDialog(HomeActivity.this);
        progressDialog.setMessage("Tunggu sebentar, sedang mendapatkan data checkin");
        progressDialog.setCancelable(false);
        progressDialog.show();

        User.instance.getCheckedInPlace(new User.ICheckedInPlaceResult() {
            @Override
            public void onResult(Place place) {
                progressDialog.hide();
                if(place == null){
                    if(placeType == Place.PlaceType.OSS_OSK || placeType == Place.PlaceType.SEKOLAH){
                        Intent intent = new Intent(HomeActivity.this, PlaceJadwalActivity.class);
                        StaticVariable.add("placeType", placeType);
                        startActivity(intent);
                    }else if(placeType == Place.PlaceType.EVENT){
                        Intent intent = new Intent(HomeActivity.this, PlaceListActivity.class);
                        intent.putExtra("command", PlaceListActivity.COMMAND_EVENT);
                        StaticVariable.add("placeType", placeType);
                        startActivity(intent);
                    }
                } else {
                    Intent intent = new Intent(HomeActivity.this, PlaceProfileActivity.class);
                    startActivity(intent);
                }
            }
        });
    }
}
