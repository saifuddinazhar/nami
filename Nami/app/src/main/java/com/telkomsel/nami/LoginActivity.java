package com.telkomsel.nami;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.telkomsel.nami.universal.core.MockLocationDetector;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StartupConfig;
import com.telkomsel.nami.universal.core.User;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    // UI references.
    private EditText etPhone;
    private EditText etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        etPhone = (EditText) findViewById(R.id.etPhone);
        etPassword = (EditText) findViewById(R.id.etPassword);
        Button btSignIn = (Button) findViewById(R.id.btSignIn);
        Button btSignInLeader = (Button) findViewById(R.id.btSignInLeader);

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE || id == EditorInfo.IME_NULL) {
                    return true;
                }
                return false;
            }
        });

        btSignIn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                final String userId = etPhone.getText().toString();
                final String password = etPassword.getText().toString();

                if (userId.isEmpty()) {
                    Toast.makeText(LoginActivity.this, "User id kosong", Toast.LENGTH_LONG).show();
                } else if (password.equals("")) {
                    Toast.makeText(LoginActivity.this, "Password kosong", Toast.LENGTH_LONG).show();
                } else {
                    final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
                    progressDialog.setMessage("Signing in...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    StartupConfig.initialize(new StartupConfig.IInitializeListener() {
                        @Override
                        public void onSuccess() {
                            ServerHandler.Parameter[] parameters = {new ServerHandler.Parameter("userid", userId), new ServerHandler.Parameter("userpassword", password)};
                            ServerHandler serverHandler = new ServerHandler("sp_user_login.php", parameters);
                            serverHandler.addListener(new ServerHandler.Listener() {
                                @Override
                                public void onSuccess(String result) {
                                    if (result.equals("false")) {
                                        progressDialog.hide();
                                        Toast.makeText(LoginActivity.this, "Username atau password salah", Toast.LENGTH_LONG).show();
                                    } else {
                                        SharedPreferences globalPreferences = getSharedPreferences("global_preferences", Context.MODE_PRIVATE);
                                        globalPreferences.edit().putString("last_login_phone", userId).putString("last_login_password", password).commit();

                                        etPhone.setText("");
                                        etPassword.setText("");
                                        progressDialog.hide();

                                        try {
                                            JSONObject jsonResult = new JSONObject(result);
                                            JSONObject jsonUser = jsonResult.getJSONObject("user");
                                            User user = new User(jsonUser);
                                            User.instance = user;

                                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                            startActivity(intent);

                                            finish();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }

                                @Override
                                public void onError(Exception error) {
                                    Toast.makeText(LoginActivity.this, "Terjadi kesalahan : " + error.toString(), Toast.LENGTH_LONG).show();
                                    Log.d("nami", error.toString());
                                }
                            });
                            serverHandler.execute();
                        }

                        @Override
                        public void onError(Exception error) {
                            Log.d("Nami", error.toString());
                            Toast.makeText(LoginActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        });

        btSignInLeader.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(LoginActivity.this, "Maaf, fitur ini masih dalam tahap pengembangan", Toast.LENGTH_SHORT).show();
            }
        });
    }
}

