package com.telkomsel.nami

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Toast
import com.telkomsel.nami.universal.core.ServerHandler
import com.telkomsel.nami.universal.core.StartupConfig
import com.telkomsel.nami.universal.core.User
import com.telkomsel.nami.universal.core.Utility
import org.json.JSONException
import org.json.JSONObject

class SplashScreenActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        if(Utility.isLocationHighAccuracyEnabled(this)){
            val globalPreferences = getSharedPreferences("global_preferences", Context.MODE_PRIVATE)
            val lastUserId = globalPreferences.getString("last_login_phone", "")
            val lastPassword = globalPreferences.getString("last_login_password", "")

            if (lastUserId !== "") {
                StartupConfig.initialize(object : StartupConfig.IInitializeListener {
                    override fun onSuccess() {
                        val parameters = arrayOf(ServerHandler.Parameter("userid", lastUserId), ServerHandler.Parameter("userpassword", lastPassword))
                        val serverHandler = ServerHandler("sp_user_login.php", parameters)
                        serverHandler.addListener(object : ServerHandler.Listener {
                            override fun onSuccess(result: String) {
                                if (result == "false") {
                                    globalPreferences.edit().remove("last_login_phone")
                                    globalPreferences.edit().remove("last_login_password")
                                    val intent = Intent(this@SplashScreenActivity, LoginActivity::class.java)
                                    startActivity(intent)
                                    finish()
                                } else {
                                    try {
                                        val jsonResult = JSONObject(result)
                                        val jsonUser = jsonResult.getJSONObject("user")
                                        val user = User(jsonUser)
                                        User.instance = user

                                        val intent = Intent(this@SplashScreenActivity, HomeActivity::class.java)
                                        startActivity(intent)
                                        finish()
                                    } catch (e: JSONException) {
                                        e.printStackTrace()
                                    }
                                }
                            }

                            override fun onError(error: Exception) {
                                Toast.makeText(this@SplashScreenActivity, "Terjadi kesalahan : " + error.toString(), Toast.LENGTH_LONG).show()
                                Log.d("nami", error.toString())
                            }
                        })
                        serverHandler.execute()
                    }

                    override fun onError(error: Exception) {
                        Log.d("Nami", error.toString())
                        Toast.makeText(this@SplashScreenActivity, error.toString(), Toast.LENGTH_SHORT).show()
                    }
                })
            } else {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
                finish()
            }
        } else {
            val alertDialog = AlertDialog.Builder(this).create()
            alertDialog.setTitle("Peringatan")
            alertDialog.setMessage("Mode High Accuracy pada setting GPS Anda belum aktif. Aktifkan terlebih dahulu untuk memulai NAMI. Masuk ke menu Setting - Location - Mode - High Accuracy. Letak menu bisa jadi berbeda pada masing-masing device")
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK") { dialog, which ->
                dialog.dismiss()
                finish()
            }
            alertDialog.show()
        }
    }
}
