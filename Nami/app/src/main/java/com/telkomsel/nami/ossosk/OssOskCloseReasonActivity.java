package com.telkomsel.nami.ossosk;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.OssOsk;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StaticVariable;
import com.telkomsel.nami.universal.core.User;

import org.json.JSONArray;
import org.json.JSONException;

public class OssOskCloseReasonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oss_osk_close_reason);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final ListView listView = (ListView)findViewById(R.id.activity_oss_osk_close_reason_listview);
        final OssOsk ossOsk = (OssOsk) StaticVariable.getAndRemove("selectedOssOsk");


        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mendapatkan data alasan tutup");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ServerHandler.Parameter [] parameters =  {};
        ServerHandler serverHandler = new ServerHandler("sp_config_visit_oss_osk_reason.php", parameters);
        serverHandler.addListener(new ServerHandler.Listener() {
            @Override
            public void onSuccess(String result) {
                progressDialog.hide();
                try {
                    JSONArray jsonReasons = new JSONArray(result);
                    final String [] reasons = new String[jsonReasons.length()];
                    for(int i=0; i<jsonReasons.length(); i++){
                        reasons[i] = jsonReasons.getString(i);
                    }
                    ArrayAdapter arrayAdapter = new ArrayAdapter(OssOskCloseReasonActivity.this, android.R.layout.simple_list_item_1, reasons);
                    listView.setAdapter(arrayAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                            final String selectedReason = reasons[i];
                            AlertDialog.Builder builder = new AlertDialog.Builder(OssOskCloseReasonActivity.this);
                            builder.setMessage("Apakah Anda yakin alasan tutup adalah " + '"' + selectedReason + '"' + " ?").setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    final ProgressDialog progressDialog = new ProgressDialog(OssOskCloseReasonActivity.this);
                                    progressDialog.setMessage("Submit alasan tutup");
                                    progressDialog.setCancelable(false);
                                    progressDialog.show();

                                    User.instance.checkInClosePlace(ossOsk, selectedReason, new User.ICheckoutResult() {
                                        @Override
                                        public void onComplete(boolean isSuccess) {
                                            progressDialog.hide();
                                            if(isSuccess){
                                                AlertDialog.Builder builder = new AlertDialog.Builder(OssOskCloseReasonActivity.this);
                                                builder.setMessage("Alasan tutup telah di submit, terima kasih").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialogInterface, int i) {
                                                        Intent returnIntent = new Intent();
                                                        setResult(Activity.RESULT_OK,returnIntent);
                                                        finish();
                                                    }
                                                }).show();
                                            }else {
                                                AlertDialog.Builder builder = new AlertDialog.Builder(OssOskCloseReasonActivity.this);
                                                builder.setMessage("Terjadi kesalahan, hubungi administrator untuk info lebih lanjut").setPositiveButton("Ok", null).show();
                                            }
                                        }
                                    });
                                }
                            }).setNegativeButton("Tidak", null).show();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Exception error) {

            }
        });
        serverHandler.execute();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
