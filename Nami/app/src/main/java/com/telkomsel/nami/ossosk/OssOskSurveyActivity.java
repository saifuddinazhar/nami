package com.telkomsel.nami.ossosk;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.OssOsk;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.User;
import com.telkomsel.nami.ossosk.fragment.OssOskSurveyBrandingFragment;
import com.telkomsel.nami.ossosk.fragment.OssOskSurveyFragment;
import com.telkomsel.nami.ossosk.fragment.OssOskSurveyPaketTerlakuFragment;
import com.telkomsel.nami.ossosk.fragment.OssOskSurveyQualitySignalFragment;
import com.telkomsel.nami.ossosk.fragment.OssOskSurveySalesBroadbandFragment;
import com.telkomsel.nami.ossosk.fragment.OssOskSurveyDisplayFragment;
import com.telkomsel.nami.ossosk.fragment.OssOskSurveyRechargeFragment;

import java.util.ArrayList;
import java.util.List;

public class OssOskSurveyActivity extends AppCompatActivity {
    OssOskSurveySalesBroadbandFragment salesBroadbandFragment;
    OssOskSurveyDisplayFragment displayFragment;
    OssOskSurveyRechargeFragment rechargeFragment;
    OssOskSurveyPaketTerlakuFragment paketTerlakuFragment;
    OssOskSurveyQualitySignalFragment qualitySignalFragment;
    OssOskSurveyBrandingFragment brandingFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oss_osk_survey);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(3);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        salesBroadbandFragment = new OssOskSurveySalesBroadbandFragment();
        displayFragment = new OssOskSurveyDisplayFragment();
        rechargeFragment = new OssOskSurveyRechargeFragment();
        paketTerlakuFragment = new OssOskSurveyPaketTerlakuFragment();
        qualitySignalFragment = new OssOskSurveyQualitySignalFragment();
        brandingFragment = new OssOskSurveyBrandingFragment();

        adapter.addFragment(salesBroadbandFragment,"SALES BROADBAND");
        adapter.addFragment(displayFragment,"DISPLAY");
        adapter.addFragment(rechargeFragment,"RECHARGE");
        adapter.addFragment(paketTerlakuFragment,"PRODUK TERLAKU");
        adapter.addFragment(qualitySignalFragment,"KUALITAS SINYAL");
        adapter.addFragment(brandingFragment,"BRANDING");

        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_barcode_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.menu_barcode_list_action_upload){
            OssOskSurveyFragment [] fragments = {salesBroadbandFragment, displayFragment, rechargeFragment,
                    paketTerlakuFragment, qualitySignalFragment, brandingFragment};

            ArrayList<ServerHandler.Parameter> parameters = new ArrayList<>();

            for (OssOskSurveyFragment fragment : fragments) {
                String validationMessage = fragment.getValidationMessage();
                if(validationMessage != null){
                    showValidationMessageWarning(validationMessage);
                    return super.onOptionsItemSelected(item);
                }

                ServerHandler.Parameter [] fragmentParameters = fragment.getServerHandlerParameters();
                for (ServerHandler.Parameter fragmentParameter:fragmentParameters) {
                    parameters.add(fragmentParameter);
                }
            }

            parameters.add(new ServerHandler.Parameter("userId",User.instance.getPhoneOrUserId()));
            parameters.add(new ServerHandler.Parameter("username",User.instance.getUsername()));
            OssOsk ossOsk = (OssOsk)User.instance.getCachedCheckinPlace();
            parameters.add(new ServerHandler.Parameter("idOutlet",ossOsk.getIdOutletSefiia()));
            parameters.add(new ServerHandler.Parameter("outletName",ossOsk.getNama()));

            final ProgressDialog progressDialog = new ProgressDialog(OssOskSurveyActivity.this);
            progressDialog.setMessage("Mengupload hasil survey ke server");
            progressDialog.setCancelable(false);
            progressDialog.show();

            ServerHandler serverHandler = new ServerHandler("sp_oss_osk_profiling_insert.php", parameters.toArray(new ServerHandler.Parameter[parameters.size()]));
            serverHandler.addListener(new ServerHandler.Listener() {
                @Override
                public void onSuccess(String result) {
                    progressDialog.hide();
                    if(result.equals("true")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(OssOskSurveyActivity.this);
                        builder.setMessage("Survey telah berhasil di upload, terima kasih").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        }).show();
                    }else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(OssOskSurveyActivity.this);
                        builder.setMessage("Terjadi kesalahan : " + result).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        }).show();
                    }
                }

                @Override
                public void onError(Exception error) {
                   error.printStackTrace();
                }
            });
            serverHandler.execute();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return  true;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getItemPosition(Object object){
            return super.getItemPosition(object);

        }
    }

    private void showValidationMessageWarning(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setPositiveButton("Ok",null).show();
    }
}
