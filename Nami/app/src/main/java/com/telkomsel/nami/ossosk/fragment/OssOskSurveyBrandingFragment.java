package com.telkomsel.nami.ossosk.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.ServerHandler;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;

/**
 * Created by saifu on 1/23/2018.
 */

public class OssOskSurveyBrandingFragment extends OssOskSurveyFragment {
    private Spinner spinnerDominantBrand;
    private Button buttonTakePhoto;
    private ArrayAdapter<CharSequence> adapterDominantBranding;
    private ImageView imagePreview;
    private Bitmap bitmap;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_oss_osk_survey_branding, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinnerDominantBrand = (Spinner)getView().findViewById(R.id.fragment_oss_osk_survey_branding_spinner_dominasi);
        buttonTakePhoto = (Button)getView().findViewById(R.id.fragment_oss_osk_survey_branding_button_photo);
        imagePreview = (ImageView) getView().findViewById(R.id.fragment_oss_osk_survey_branding_imageview_photo);

        ServerHandler serverHandler = new ServerHandler("sp_config_branding.php", new ServerHandler.Parameter[0]);
        serverHandler.addListener(new ServerHandler.Listener() {
            @Override
            public void onSuccess(String result) {
                try {
                    JSONArray jsonDominants = new JSONArray(result);
                    final String [] dominants = new String[jsonDominants.length()];

                    for(int i=0; i<jsonDominants.length(); i++){
                        dominants[i] = jsonDominants.getString(i);
                    }

                    adapterDominantBranding = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.simple_spinner_item);
                    adapterDominantBranding.add("--Pilih--");
                    for (String dominantBrand: dominants) {
                        adapterDominantBranding.add(dominantBrand);
                    }
                    spinnerDominantBrand.setAdapter(adapterDominantBranding);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Exception error) {
                error.printStackTrace();
            }
        });
        serverHandler.execute();

        buttonTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ( ContextCompat.checkSelfPermission( getActivity(), Manifest.permission.CAMERA ) == PackageManager.PERMISSION_GRANTED ) {
                    getPhoto();
                }else {
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Peringatan");
                    alertDialog.setMessage("Camera Permission dibutuhkan untuk mengambil gambar branding");
                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            OssOskSurveyBrandingFragment.this.requestPermissions(new String[] {  Manifest.permission.CAMERA  }, 10); ;
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 10){
            
            if(grantResults[0] == -1){
                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Peringatan");
                alertDialog.setMessage("Anda tidak dapat mengambil gambar karena permission camera ditolak. Masuk ke setting aplikasi, pilih Nami, dan aktifkan Camera permission");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            } else if(grantResults[0] == 0){
                getPhoto();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 10 && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            bitmap = (Bitmap) extras.get("data");
            imagePreview.setImageBitmap(bitmap);
        }
    }

    @Override
    public String getValidationMessage() {
        if(spinnerDominantBrand.getSelectedItem().toString().equals("--Pilih--")){
            return "Dominant branding pada menu BRANDING belum dipilih, silahkan cek ulang";
        }else if(bitmap == null){
            return "Anda belum mengambil foto outlet pada menu BRANDING";
        }
        return super.getValidationMessage();
    }

    @Override
    public ServerHandler.Parameter[] getServerHandlerParameters() {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream); //compress to which format you want.
        byte [] imageBytes = stream.toByteArray();
        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

        ServerHandler.Parameter [] parameters = {
            new ServerHandler.Parameter("dominantBranding", spinnerDominantBrand.getSelectedItem().toString()),
            new ServerHandler.Parameter("imageData", imageString)
        };
        return parameters;
    }

    private void getPhoto(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, 10);
        }
    }
}