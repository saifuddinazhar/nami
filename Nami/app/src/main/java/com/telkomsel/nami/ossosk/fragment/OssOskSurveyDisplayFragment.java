package com.telkomsel.nami.ossosk.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StartupConfig;


public class OssOskSurveyDisplayFragment extends OssOskSurveyFragment {
    private EditText editTextTelkomsel, editTextOoredoo, editTextXl, editTextAxis, editText3, editTextOther;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_oss_osk_survey_display, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editTextTelkomsel = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_display_edit_text_telkomsel);
        editTextOoredoo = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_display_edit_text_ooredoo);
        editTextXl = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_display_edit_text_xl);
        editTextAxis = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_display_edit_text_axis);
        editText3 = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_display_edit_text_3);
        editTextOther = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_display_edit_text_other);
    }

    @Override
    public String getValidationMessage() {
        EditText [] editTexts = {editTextTelkomsel, editTextOoredoo, editTextXl, editTextAxis, editText3, editTextOther};
        for (EditText editText : editTexts) {
            String text = editText.getText().toString();
            if(text.equals("")){
                return "Salah satu atau lebih isian pada menu DISPLAY kosong, pastikan semua data terisi";
            }else {
                int textInt = Integer.parseInt(text);
                if(textInt > StartupConfig.getOssOskSurveyDisplay()){
                    return "Jumlah display pada menu DISPLAY tidak boleh melebihi 200, harap isi dengan sebenarnya";
                }
            }
        }

        return super.getValidationMessage();
    }

    @Override
    public ServerHandler.Parameter[] getServerHandlerParameters() {
        ServerHandler.Parameter [] parameters = {
                new ServerHandler.Parameter("displayTelkomsel", editTextTelkomsel.getText().toString()),
                new ServerHandler.Parameter("displayOoredoo", editTextOoredoo.getText().toString()),
                new ServerHandler.Parameter("displayXl", editTextXl.getText().toString()),
                new ServerHandler.Parameter("displayAxis", editTextAxis.getText().toString()),
                new ServerHandler.Parameter("display3", editText3.getText().toString()),
                new ServerHandler.Parameter("displayOther", editTextOther.getText().toString()),
        };
        return parameters;
    }
}
