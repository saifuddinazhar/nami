package com.telkomsel.nami.ossosk.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StartupConfig;

/**
 * Created by saifu on 1/23/2018.
 */

public class OssOskSurveyPaketTerlakuFragment extends OssOskSurveyFragment {
    private EditText editTextQuotaTelkomsel, editTextQuotaOoredoo, editTextQuotaXl, editTextQuotaAxis, editTextQuota3, editTextQuotaOther;
    private EditText editTextTpTelkomsel, editTextTpOoredoo, editTextTpXl, editTextTpAxis, editTextTp3, editTextTpOther;
    private EditText editTextEupTelkomsel, editTextEupOoredoo, editTextEupXl, editTextEupAxis, editTextEup3, editTextEupOther;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_oss_osk_survey_paket_terlaku, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editTextQuotaTelkomsel = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_quota_telkomsel);
        editTextQuotaOoredoo = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_quota_ooredoo);
        editTextQuotaXl = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_quota_xl);
        editTextQuotaAxis = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_quota_axis);
        editTextQuota3 = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_quota_3);
        editTextQuotaOther = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_quota_other);

        editTextTpTelkomsel = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_tp_telkomsel);
        editTextTpOoredoo = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_tp_ooredoo);
        editTextTpXl = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_tp_xl);
        editTextTpAxis = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_tp_axis);
        editTextTp3 = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_tp_3);
        editTextTpOther = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_tp_other);

        editTextEupTelkomsel = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_eup_telkomsel);
        editTextEupOoredoo = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_eup_ooredoo);
        editTextEupXl = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_eup_xl);
        editTextEupAxis = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_eup_axis);
        editTextEup3 = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_eup_3);
        editTextEupOther = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_terlaku_edit_text_eup_other);
    }

    @Override
    public String getValidationMessage() {
        EditText [] editTextQuotas = {editTextQuotaTelkomsel, editTextQuotaOoredoo, editTextQuotaXl, editTextQuotaAxis, editTextQuota3, editTextQuotaOther};
        EditText [] editTextTps = {editTextTpTelkomsel, editTextTpOoredoo, editTextTpXl, editTextTpAxis, editTextTp3, editTextTpOther};
        EditText [] editTextEups = {editTextEupTelkomsel, editTextEupOoredoo, editTextEupXl, editTextEupAxis, editTextEup3, editTextEupOther};

        for(int i=0; i<editTextTps.length; i++){
            if(editTextQuotas[i].getText().toString().equals("") || editTextTps[i].getText().toString().equals("") || editTextEups[i].getText().toString().equals("")){
                return "Salah satu atau lebih isian pada menu PRODUK TERLAKU kosong, pastikan semua data terisi";
            } else if(Float.parseFloat(editTextQuotas[i].getText().toString()) > StartupConfig.getOssOskSurveyProdukTerlakuQuota()){
                return "Salah satu atau lebih isian Quota pada menu PRODUK TERLAKU melebihi " + StartupConfig.getOssOskSurveyProdukTerlakuQuota() + ", harap isi dengan sebenarnya";
            }else {
                float tp = Float.parseFloat(editTextTps[i].getText().toString());
                float eup = Float.parseFloat(editTextEups[i].getText().toString());

                if(tp > StartupConfig.getOssOskSurveyProdukTerlakuTp()){
                    return "TP tidak boleh lebih dari " + StartupConfig.getOssOskSurveyProdukTerlakuTp() + ", cek kembali pada menu PRODUK TERLAKU";
                }else if(eup < tp){
                    return "EUP tidak boleh kurang dari TP, cek kembali pada menu PRODUK TERLAKU";
                }
            }
        }

        return super.getValidationMessage();
    }

    @Override
    public ServerHandler.Parameter[] getServerHandlerParameters() {
        ServerHandler.Parameter [] parameters = {
                new ServerHandler.Parameter("quotaTelkomsel", editTextQuotaTelkomsel.getText().toString()),
                new ServerHandler.Parameter("quotaOoredoo", editTextQuotaOoredoo.getText().toString()),
                new ServerHandler.Parameter("quotaXl", editTextQuotaXl.getText().toString()),
                new ServerHandler.Parameter("quotaAxis", editTextQuotaAxis.getText().toString()),
                new ServerHandler.Parameter("quota3", editTextQuota3.getText().toString()),
                new ServerHandler.Parameter("quotaOther", editTextQuotaOther.getText().toString()),

                new ServerHandler.Parameter("tpTelkomsel", editTextTpTelkomsel.getText().toString()),
                new ServerHandler.Parameter("tpOoredoo", editTextTpOoredoo.getText().toString()),
                new ServerHandler.Parameter("tpXl", editTextTpXl.getText().toString()),
                new ServerHandler.Parameter("tpAxis", editTextTpAxis.getText().toString()),
                new ServerHandler.Parameter("tp3", editTextTp3.getText().toString()),
                new ServerHandler.Parameter("tpOther", editTextTpOther.getText().toString()),

                new ServerHandler.Parameter("eupTelkomsel", editTextEupTelkomsel.getText().toString()),
                new ServerHandler.Parameter("eupOoredoo", editTextEupOoredoo.getText().toString()),
                new ServerHandler.Parameter("eupXl", editTextEupXl.getText().toString()),
                new ServerHandler.Parameter("eupAxis", editTextEupAxis.getText().toString()),
                new ServerHandler.Parameter("eup3", editTextEup3.getText().toString()),
                new ServerHandler.Parameter("eupOther", editTextEupOther.getText().toString()),
        };
        return parameters;
    }
}
