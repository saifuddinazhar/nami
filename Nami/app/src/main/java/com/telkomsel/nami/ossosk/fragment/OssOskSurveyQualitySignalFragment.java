package com.telkomsel.nami.ossosk.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.ServerHandler;

/**
 * Created by saifu on 1/23/2018.
 */

public class OssOskSurveyQualitySignalFragment extends OssOskSurveyFragment {
    private RadioGroup spinnerTelkomselVoice, spinnerOoredooVoice, spinnerXlVoice, spinnerAxisVoice, spinner3Voice, spinnerOtherVoice;
    private RadioGroup spinnerTelkomsel3g, spinnerOoredoo3g, spinnerXl3g, spinnerAxis3g, spinner33g, spinnerOther3g;
    private RadioGroup spinnerTelkomsel4g, spinnerOoredoo4g, spinnerXl4g, spinnerAxis4g, spinner34g, spinnerOther4g;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_oss_osk_survey_quality_signal, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        spinnerTelkomselVoice = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_telkomsel_voice);
        spinnerTelkomsel3g = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_telkomsel_3g);
        spinnerTelkomsel4g = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_telkomsel_4g);

        spinnerOoredooVoice = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_ooredoo_voice);
        spinnerOoredoo3g = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_ooredoo_3g);
        spinnerOoredoo4g = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_ooredoo_4g);

        spinnerXlVoice = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_xl_voice);
        spinnerXl3g = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_xl_3g);
        spinnerXl4g = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_xl_4g);

        spinnerAxisVoice = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_axis_voice);
        spinnerAxis3g = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_axis_3g);
        spinnerAxis4g = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_axis_4g);

        spinner3Voice = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_3_voice);
        spinner33g = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_3_3g);
        spinner34g = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_3_4g);

        spinnerOtherVoice = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_other_voice);
        spinnerOther3g = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_other_3g);
        spinnerOther4g = (RadioGroup)getView().findViewById(R.id.fragment_oss_osk_survey_quality_signal_spinner_other_4g);
    }

    @Override
    public String getValidationMessage() {
        RadioGroup [] spinnerVoices = {spinnerTelkomselVoice, spinnerOoredooVoice, spinnerXlVoice, spinnerAxisVoice, spinner3Voice, spinnerOtherVoice};
        RadioGroup [] spinner3Gs = {spinnerTelkomsel3g, spinnerOoredoo3g, spinnerXl3g, spinnerAxis3g, spinner33g, spinnerOther3g};
        RadioGroup [] spinner4Gs = {spinnerTelkomsel4g, spinnerOoredoo4g, spinnerXl4g, spinnerAxis4g, spinner34g, spinnerOther4g};

        for(int i=0; i<spinnerVoices.length; i++){
            RadioGroup spinnerVoice = spinnerVoices[i];
            RadioGroup spinner3G = spinner3Gs[i];
            RadioGroup spinner4G = spinner4Gs[i];

            if(spinnerVoice.getCheckedRadioButtonId() == -1 ||
                    spinner3G.getCheckedRadioButtonId() == -1 ||
                    spinner4G.getCheckedRadioButtonId() == -1){

                return "Salah satu atau lebih isian pada menu SINYAL kosong, pastikan semua data terisi";
            }
        }

        return super.getValidationMessage();
    }

    @Override
    public ServerHandler.Parameter[] getServerHandlerParameters() {
        ServerHandler.Parameter [] parameters = {
            new ServerHandler.Parameter("networkTelkomsel", "voice:"
                    + ((RadioButton)getView().findViewById(spinnerTelkomselVoice.getCheckedRadioButtonId())).getText().toString() + "/3g:"
                    + ((RadioButton)getView().findViewById(spinnerTelkomsel3g.getCheckedRadioButtonId())).getText().toString() + "/4g:"
                    + ((RadioButton)getView().findViewById(spinnerTelkomselVoice.getCheckedRadioButtonId())).getText().toString()),
            new ServerHandler.Parameter("networkOoredoo", "voice:"
                    + ((RadioButton)getView().findViewById(spinnerTelkomselVoice.getCheckedRadioButtonId())).getText().toString() + "/3g:"
                    + ((RadioButton)getView().findViewById(spinnerOoredoo3g.getCheckedRadioButtonId())).getText().toString() + "/4g:"
                    + ((RadioButton)getView().findViewById(spinnerOoredoo4g.getCheckedRadioButtonId())).getText().toString()),
            new ServerHandler.Parameter("networkXl", "voice:"
                    + ((RadioButton)getView().findViewById(spinnerXlVoice.getCheckedRadioButtonId())).getText().toString() + "/3g:"
                    + ((RadioButton)getView().findViewById(spinnerXl3g.getCheckedRadioButtonId())).getText().toString() + "/4g:"
                    + ((RadioButton)getView().findViewById(spinnerXl4g.getCheckedRadioButtonId())).getText().toString()),
            new ServerHandler.Parameter("networkAxis", "voice:"
                    + ((RadioButton)getView().findViewById(spinnerAxisVoice.getCheckedRadioButtonId())).getText().toString() + "/3g:"
                    + ((RadioButton)getView().findViewById(spinnerAxis3g.getCheckedRadioButtonId())).getText().toString() + "/4g:"
                    + ((RadioButton)getView().findViewById(spinnerAxis4g.getCheckedRadioButtonId())).getText().toString()),
            new ServerHandler.Parameter("network3", "voice:"
                    + ((RadioButton)getView().findViewById(spinner3Voice.getCheckedRadioButtonId())).getText().toString() + "/3g:"
                    + ((RadioButton)getView().findViewById(spinner33g.getCheckedRadioButtonId())).getText().toString() + "/4g:"
                    + ((RadioButton)getView().findViewById(spinner34g.getCheckedRadioButtonId())).getText().toString()),
            new ServerHandler.Parameter("networkOther", "voice:"
                    + ((RadioButton)getView().findViewById(spinnerOtherVoice.getCheckedRadioButtonId())).getText().toString() + "/3g:"
                    + ((RadioButton)getView().findViewById(spinnerOther3g.getCheckedRadioButtonId())).getText().toString() + "/4g:"
                    + ((RadioButton)getView().findViewById(spinnerOther4g.getCheckedRadioButtonId())).getText().toString())
        };
        return parameters;
    }
}
