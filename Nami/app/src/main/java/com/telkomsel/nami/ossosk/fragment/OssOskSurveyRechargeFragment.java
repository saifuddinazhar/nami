package com.telkomsel.nami.ossosk.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StartupConfig;


/**
 * A simple {@link Fragment} subclass.
 */
public class OssOskSurveyRechargeFragment extends OssOskSurveyFragment {
    private EditText editTextTelkomsel, editTextOoredoo, editTextXl, editTextAxis, editText3, editTextOther;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_oss_osk_survey_recharge, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        editTextTelkomsel = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_recharge_edit_text_telkomsel);
        editTextOoredoo = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_recharge_edit_text_ooredoo);
        editTextXl = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_recharge_edit_text_xl);
        editTextAxis = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_recharge_edit_text_axis);
        editText3 = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_recharge_edit_text_3);
        editTextOther = (EditText)getView().findViewById(R.id.fragment_oss_osk_survey_recharge_edit_text_other);
    }

    @Override
    public String getValidationMessage() {
        EditText [] editTexts = {editTextTelkomsel, editTextOoredoo, editTextXl, editTextAxis, editText3, editTextOther};
        for (EditText editText : editTexts) {
            String text = editText.getText().toString();
            if(text.equals("")){
                return "Salah satu atau lebih isian pada menu RECHARGE kosong, pastikan semua data terisi";
            }else {
                int textInt = Integer.parseInt(text);
                if(textInt > StartupConfig.getOssOskSurveyRecharge()){
                    return "Jumlah penjualan pada menu RECHARGE tidak boleh melebihi " + StartupConfig.getOssOskSurveyRecharge() + " transaksi, harap isi dengan sebenarnya.";
                }
            }
        }
        return super.getValidationMessage();
    }

    @Override
    public ServerHandler.Parameter[] getServerHandlerParameters() {
        ServerHandler.Parameter [] parameters = {
            new ServerHandler.Parameter("rechargeTelkomsel", editTextTelkomsel.getText().toString()),
            new ServerHandler.Parameter("rechargeOoredoo", editTextOoredoo.getText().toString()),
            new ServerHandler.Parameter("rechargeXl", editTextXl.getText().toString()),
            new ServerHandler.Parameter("rechargeAxis", editTextAxis.getText().toString()),
            new ServerHandler.Parameter("recharge3", editText3.getText().toString()),
            new ServerHandler.Parameter("rechargeOther", editTextOther.getText().toString()),
        };
        return parameters;
    }
}
