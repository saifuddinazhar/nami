package com.telkomsel.nami.sekolah;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.telkomsel.nami.R;
import com.telkomsel.nami.ossosk.fragment.OssOskSurveyBrandingFragment;
import com.telkomsel.nami.universal.core.Sekolah;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StartupConfig;
import com.telkomsel.nami.universal.core.StaticVariable;
import com.telkomsel.nami.universal.core.User;
import com.telkomsel.nami.universal.core.Utility;
import org.json.JSONArray;

public class SekolahCanteenEditActivity extends AppCompatActivity {
    private Sekolah.Canteen canteenToEdit = null;
    private boolean isInsert;
    private EditText editTextCanteenName, editTextOwnerName, editTextOwnerContact, editTextMKios;
    private TextView textViewCanteenName;
    private TextView textViewShopsign, textViewTableCloth, textViewSachet;
    private Spinner spinnerShopSign, spinnerTableCloth;
    private EditText editTextSachet;
    private TextView textViewPhoto;
    private ImageView imageViewPhoto;
    private Button buttonPhoto;

    private Bitmap bitmapPhoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sekolah_canteen_edit);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        editTextCanteenName = (EditText)findViewById(R.id.activity_sekolah_canteen_edit_canteen_name);
        editTextOwnerName = (EditText)findViewById(R.id.activity_sekolah_canteen_edit_owner_name);
        editTextOwnerContact = (EditText)findViewById(R.id.activity_sekolah_canteen_edit_owner_contact);
        editTextMKios = (EditText)findViewById(R.id.activity_sekolah_canteen_edit_rsm_kios);
        textViewCanteenName = (TextView)findViewById(R.id.activity_sekolah_canteen_edit_textview_canteen_name);


        textViewShopsign = (TextView)findViewById(R.id.activity_sekolah_canteen_textview_shopsign);
        textViewTableCloth = (TextView)findViewById(R.id.activity_sekolah_canteen_textview_tablecloth);
        textViewSachet = (TextView)findViewById(R.id.activity_sekolah_canteen_textview_sachet);

        spinnerShopSign = (Spinner)findViewById(R.id.activity_sekolah_canteen_spinner_shopsign);
        spinnerTableCloth = (Spinner)findViewById(R.id.activity_sekolah_canteen_spinner_tablecloth);
        editTextSachet = (EditText)findViewById(R.id.activity_sekolah_canteen_edit_sachet);

        textViewPhoto = findViewById(R.id.activity_sekolah_canteen_edit_textview_photo);
        imageViewPhoto = findViewById(R.id.activity_sekolah_canteen_edit_imageview_photo);
        buttonPhoto = findViewById(R.id.activity_sekolah_canteen_edit_button_photo);

        isInsert = getIntent().getBooleanExtra("isInsert", false);

        if(isInsert){
            getSupportActionBar().setTitle("Tambah Kantin");

            editTextCanteenName.setVisibility(View.VISIBLE);
            textViewCanteenName.setVisibility(View.GONE);
            textViewShopsign.setVisibility(View.GONE);
            textViewTableCloth.setVisibility(View.GONE);
            textViewSachet.setVisibility(View.GONE);
            spinnerShopSign.setVisibility(View.GONE);
            spinnerTableCloth.setVisibility(View.GONE);
            editTextSachet.setVisibility(View.GONE);
            textViewPhoto.setVisibility(View.GONE);
            imageViewPhoto.setVisibility(View.GONE);
            buttonPhoto.setVisibility(View.GONE);
        }else {
            getSupportActionBar().setTitle("Edit Kantin");

            editTextCanteenName.setVisibility(View.GONE);
            textViewCanteenName.setVisibility(View.VISIBLE);

            canteenToEdit = (Sekolah.Canteen) StaticVariable.getAndRemove("selectedCanteen");

            textViewCanteenName.setText(canteenToEdit.getName());
            editTextOwnerName.setText(canteenToEdit.getOwnerName());
            editTextOwnerContact.setText(Utility.convertPhoneNumberToStandardLocalPhoneNmber(canteenToEdit.getOwnerContact()));
            editTextMKios.setText(Utility.convertPhoneNumberToStandardLocalPhoneNmber(canteenToEdit.getMKios()));

            ServerHandler serverHandler = new ServerHandler("sp_config_branding.php", new ServerHandler.Parameter[0]);
            serverHandler.addListener(new ServerHandler.Listener() {
                @Override
                public void onSuccess(String result) {
                    try {
                        JSONArray jsonDominants = new JSONArray(result);
                        final String[] dominants = new String[jsonDominants.length()];

                        for (int i = 0; i < jsonDominants.length(); i++) {
                            dominants[i] = jsonDominants.getString(i);
                        }

                        ArrayAdapter adapterDominantBranding = new ArrayAdapter<CharSequence>(SekolahCanteenEditActivity.this, android.R.layout.simple_spinner_item);
                        adapterDominantBranding.add("--Pilih--");
                        for (String dominantBrand: dominants) {
                            adapterDominantBranding.add(dominantBrand);
                        }
                        spinnerShopSign.setAdapter(adapterDominantBranding);
                        spinnerTableCloth.setAdapter(adapterDominantBranding);
                    } catch (Exception e) {
                        Log.e("Nami", e.toString());
                    }
                }

                @Override
                public void onError(Exception error) {
                    Log.e("Nami", error.toString());
                    Toast.makeText(SekolahCanteenEditActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                }
            });
            serverHandler.execute();

            buttonPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if ( ContextCompat.checkSelfPermission(SekolahCanteenEditActivity.this, Manifest.permission.CAMERA ) == PackageManager.PERMISSION_GRANTED ) {
                        getPhoto();
                    } else if(Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
                        getPhoto();
                    } else {
                        AlertDialog alertDialog = new AlertDialog.Builder(SekolahCanteenEditActivity.this).create();
                        alertDialog.setTitle("Peringatan");
                        alertDialog.setMessage("Camera Permission dibutuhkan untuk mengambil gambar branding");
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                    requestPermissions(new String[] {  Manifest.permission.CAMERA  }, 10);
                                };
                                dialog.dismiss();
                            }
                        });
                        alertDialog.show();
                    }
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 10 && resultCode == Activity.RESULT_OK) {
            Bundle extras = data.getExtras();
            bitmapPhoto = (Bitmap) extras.get("data");
            imageViewPhoto.setImageBitmap(bitmapPhoto);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_canteen_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.menu_canteen_edit_action_simpan){
            boolean isError = false;

            if(editTextOwnerName.getText().toString().equals("") || editTextOwnerContact.getText().toString().equals("")){
                Toast.makeText(SekolahCanteenEditActivity.this, "Satu atau lebih data masih belum diisi, silahkan teliti kembali", Toast.LENGTH_SHORT).show();
                isError = true;
            }else if(Utility.convertPhoneNumberToStandardLocalPhoneNmber(editTextOwnerContact.getText().toString()) == null){
                Toast.makeText(SekolahCanteenEditActivity.this, "Nomor kontak pemilik tidak valid, mohon cek kembali", Toast.LENGTH_SHORT).show();
                isError = true;
            }else if( !editTextMKios.getText().toString().equals("") && Utility.detectOperatorFromPhoneNumber(editTextMKios.getText().toString()) != Utility.Operator.TELKOMSEL){
                Toast.makeText(SekolahCanteenEditActivity.this, "Nomor MKios harus nomor telkomsel", Toast.LENGTH_SHORT).show();
                isError = true;
            } else{
                if(isInsert){
                    if(editTextCanteenName.getText().toString().equals("")){
                        Toast.makeText(SekolahCanteenEditActivity.this, "Satu atau lebih data masih belum diisi, silahkan teliti kembali", Toast.LENGTH_SHORT).show();
                        isError = true;
                    }
                } else {
                    if(Integer.parseInt(editTextSachet.getText().toString()) > StartupConfig.getSekolahCanteenSachet()){
                        Toast.makeText(SekolahCanteenEditActivity.this, "Sachet tidak boleh melebihi " + StartupConfig.getSekolahCanteenSachet() + ", mohon isi dengan sebenarnya", Toast.LENGTH_SHORT).show();
                        isError = true;
                    }else if(spinnerShopSign.getSelectedItem().toString().equals("--Pilih--") || spinnerTableCloth.getSelectedItem().toString().equals("--Pilih--") || editTextSachet.getText().toString().equals("")){
                        Toast.makeText(SekolahCanteenEditActivity.this, "Satu atau lebih data masih belum diisi, silahkan teliti kembali", Toast.LENGTH_SHORT).show();
                        isError = true;
                    } else if(bitmapPhoto == null){
                        Toast.makeText(SekolahCanteenEditActivity.this, "Foto lokasi belum diinputkan, silahkan masukkan foto lokasi terlebih dahulu", Toast.LENGTH_SHORT).show();
                        isError = true;
                    }

                }
            }

            if(!isError){
                final ProgressDialog progressDialog = new ProgressDialog(SekolahCanteenEditActivity.this);
                progressDialog.setMessage("Tunggu sebentar.. sedang menyimpan data kantin");
                progressDialog.setCancelable(false);
                progressDialog.show();

                if(isInsert){
                    Sekolah sekolah = (Sekolah)User.instance.getCachedCheckinPlace();
                    sekolah.AddCanteen(editTextCanteenName.getText().toString(), editTextOwnerName.getText().toString(), Utility.convertPhoneNumberToStandardLocalPhoneNmber(editTextOwnerContact.getText().toString()),  Utility.convertPhoneNumberToStandardLocalPhoneNmber(editTextMKios.getText().toString()), new Sekolah.OnAddCanteenResult() {
                        @Override
                        public void onSuccess() {
                            progressDialog.hide();
                            Toast.makeText(SekolahCanteenEditActivity.this, "Kantin telah disimpan", Toast.LENGTH_SHORT).show();
                            setResult(RESULT_OK);
                            finish();
                        }

                        @Override
                        public void onError(Exception error) {
                            error.printStackTrace();
                            if(error.getMessage().equals("DuplicateCanteenName")){
                                Toast.makeText(SekolahCanteenEditActivity.this, "Terjadi kesalahan, nama kantin tidak boleh sama dengan sebelumnya", Toast.LENGTH_SHORT).show();
                            }else {
                                Toast.makeText(SekolahCanteenEditActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                            }
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    });
                } else {
                    canteenToEdit.update(textViewCanteenName.getText().toString(), editTextOwnerName.getText().toString(), Utility.convertPhoneNumberToStandardLocalPhoneNmber(editTextOwnerContact.getText().toString()), Utility.convertPhoneNumberToStandardLocalPhoneNmber(editTextMKios.getText().toString()),
                            spinnerShopSign.getSelectedItem().toString(), spinnerTableCloth.getSelectedItem().toString(), Integer.parseInt(editTextSachet.getText().toString()), bitmapPhoto, new Sekolah.Canteen.OnUpdateResult() {

                        @Override
                        public void onSuccess() {
                            progressDialog.hide();
                            Toast.makeText(SekolahCanteenEditActivity.this, "Kantin telah disimpan", Toast.LENGTH_SHORT).show();
                            setResult(RESULT_OK);
                            finish();
                        }

                        @Override
                        public void onError(Exception error) {
                            Toast.makeText(SekolahCanteenEditActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    });
                }
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        setResult(RESULT_CANCELED);
        finish();
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == 10){
            if(grantResults[0] == -1){
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Peringatan");
                alertDialog.setMessage("Anda tidak dapat mengambil gambar karena permission camera ditolak. Masuk ke setting aplikasi, pilih Nami, dan aktifkan Camera permission");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertDialog.show();
            } else if(grantResults[0] == 0){
                getPhoto();
            }
        }
    }

    private void getPhoto(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, 10);
        }
    }
}
