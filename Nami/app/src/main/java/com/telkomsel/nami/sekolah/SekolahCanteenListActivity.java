package com.telkomsel.nami.sekolah;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.telkomsel.nami.LoginActivity;
import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.BarcodePaketActivity;
import com.telkomsel.nami.universal.core.Sekolah;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StaticVariable;
import com.telkomsel.nami.universal.core.User;

import java.util.List;

public class SekolahCanteenListActivity extends AppCompatActivity {
    private String property = null;
    private Sekolah.Canteen[] canteens;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sekolah_canteen_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        property = getIntent().getStringExtra("property");

        listView = (ListView)findViewById(R.id.activity_sekolah_canteen_list_listview);
        refreshCanteen();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            Sekolah.Canteen selectedCanteen = canteens[i];
            StaticVariable.add("selectedCanteen", selectedCanteen);
            if(property == null){
                Intent intent = new Intent(SekolahCanteenListActivity.this, SekolahCanteenEditActivity.class);
                startActivityForResult(intent, 20);
            }else if(property.equals("sales")){
                Intent intent = new Intent(SekolahCanteenListActivity.this, BarcodePaketActivity.class);
                intent.putExtra("channelType", "INSTITUTION_BY_CANTEEN");
                StaticVariable.add("sp_sales_batch_insert__channeltypeid", selectedCanteen.getId());
                startActivity(intent);
            }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(property == null){
            getMenuInflater().inflate(R.menu.menu_canteen_list, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.menu_canteen_list_action_tambah){
            Intent intent = new Intent(SekolahCanteenListActivity.this, SekolahCanteenEditActivity.class);
            intent.putExtra("isInsert", true);
            startActivityForResult(intent, 10);
        }

        return super.onOptionsItemSelected(item);
    }

        @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if((requestCode == 10 || requestCode == 20) && resultCode == RESULT_OK){
            refreshCanteen();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void refreshCanteen(){
        final ProgressDialog progressDialog = new ProgressDialog(SekolahCanteenListActivity.this);
        progressDialog.setMessage("Tunggu sebentar.. Sedang mengunduh data kantin sekolah");
        progressDialog.setCancelable(false);
        progressDialog.show();

        Sekolah sekolah = (Sekolah) User.instance.getCachedCheckinPlace();
        sekolah.getCanteens(new Sekolah.OnGetCanteenObtained() {
            @Override
            public void onSuccess(Sekolah.Canteen[] obtainedCanteens) {
                progressDialog.hide();
                canteens = obtainedCanteens;
                String [] canteenNames = new String[canteens.length];
                if(canteens.length > 0){
                    for(int i=0; i<canteens.length; i++){
                        canteenNames[i] = canteens[i].getName();
                    }
                }

                ArrayAdapter arrayAdapter = new ArrayAdapter(SekolahCanteenListActivity.this, android.R.layout.simple_list_item_1, canteenNames);
                listView.setAdapter(arrayAdapter);
            }

            @Override
            public void onError(Exception error) {
                error.printStackTrace();
            }
        });
    }
}
