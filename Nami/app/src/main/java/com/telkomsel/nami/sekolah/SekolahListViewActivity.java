package com.telkomsel.nami.sekolah;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.BarcodePaketActivity;
import com.telkomsel.nami.universal.arrayadapter.ImageTextArrayAdapter;
import com.telkomsel.nami.universal.core.Place;

public class SekolahListViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sekolah_list_view);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ListView listView = (ListView) findViewById(R.id.activity_sekolah_sales_channel_listview);
        ImageTextArrayAdapter.ImageTextItem[] items = null;

        final String property = getIntent().getStringExtra("property");
        if(property.equals("sales_channel")){
            getSupportActionBar().setTitle("Penjualan");
            items = new ImageTextArrayAdapter.ImageTextItem[]{
                new ImageTextArrayAdapter.ImageTextItem(R.drawable.shop, "Kantin / Koperasi"),
                new ImageTextArrayAdapter.ImageTextItem(R.drawable.user, "End User")
            };
        }

        ImageTextArrayAdapter adapter = new ImageTextArrayAdapter(this, items);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(property.equals("sales_channel")){

                    if(i == 0){
                        Intent intent = new Intent(SekolahListViewActivity.this, SekolahCanteenListActivity.class);
                        intent.putExtra("property", "sales");
                        startActivity(intent);
                    }else if(i == 1){
                        Intent intent = new Intent(SekolahListViewActivity.this, BarcodePaketActivity.class);
                        intent.putExtra("channelType", "INSTITUTION_BY_END_USER");
                        startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
