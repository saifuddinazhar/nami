package com.telkomsel.nami.sekolah;

import android.app.ProgressDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.ui.CustomRadioGroup;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StartupConfig;
import com.telkomsel.nami.universal.core.User;
import com.telkomsel.nami.universal.core.Utility;

public class SekolahMarketShareActivity extends AppCompatActivity {
    private CustomRadioGroup customRadioGroupLegacy;
    private CustomRadioGroup customRadioGroupBroadband;
    private EditText editTextMsisdn, editTextAmount, editTextTransaksi;
    private CheckBox checkBoxMusicSpotify, checkBoxMusicJoox, checkBoxMusicLangitMusic;
    private CheckBox checkBoxVideoViu, checkBoxVideoHooq, checkBoxVideoNetflix, checkBoxVideoIflix;
    private CheckBox checkBoxGameMobileLegend, checkBoxGameAOV, checkBoxGameVainGlory, checkBoxGameGetRich, checkBoxGameCOC, checkBoxGameClashRoyale;
    private CheckBox checkBoxSosmedFacebook, checkBoxSosmedInstagram, checkBoxSosmedPath, checkBoxSosmedBBM, checkBoxSosmedLine;
    private RadioGroup radioGroupTcash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sekolah_market_share);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Legacy
        RadioButton radioButtonLegacyTelkomsel = (RadioButton)findViewById(R.id.activity_sekolah_market_share_radiobutton_legacy_telkomsel);
        RadioButton radioButtonLegacyOoredoo = (RadioButton)findViewById(R.id.activity_sekolah_market_share_radiobutton_legacy_ooredoo);
        RadioButton radioButtonLegacyXl = (RadioButton)findViewById(R.id.activity_sekolah_market_share_radiobutton_legacy_xl);
        RadioButton radioButtonLegacyAxis = (RadioButton)findViewById(R.id.activity_sekolah_market_share_radiobutton_legacy_axis);
        RadioButton radioButtonLegacy3 = (RadioButton)findViewById(R.id.activity_sekolah_market_share_radiobutton_legacy_3);
        RadioButton radioButtonLegacyOther = (RadioButton)findViewById(R.id.activity_sekolah_market_share_radiobutton_legacy_other);

        customRadioGroupLegacy = new CustomRadioGroup();
        customRadioGroupLegacy.add(radioButtonLegacyTelkomsel);
        customRadioGroupLegacy.add(radioButtonLegacyOoredoo);
        customRadioGroupLegacy.add(radioButtonLegacyXl);
        customRadioGroupLegacy.add(radioButtonLegacyAxis);
        customRadioGroupLegacy.add(radioButtonLegacy3);
        customRadioGroupLegacy.add(radioButtonLegacyOther);

        //Broadband
        RadioButton radioButtonBroadbandTelkomsel = findViewById(R.id.activity_sekolah_market_share_radiobutton_internet_telkomsel);
        RadioButton radioButtonBroadbandOoredoo = findViewById(R.id.activity_sekolah_market_share_radiobutton_internet_ooredoo);
        RadioButton radioButtonBroadbandXl = findViewById(R.id.activity_sekolah_market_share_radiobutton_internet_xl);
        RadioButton radioButtonBroadbandAxis = findViewById(R.id.activity_sekolah_market_share_radiobutton_internet_axis);
        RadioButton radioButtonBroadband3 = findViewById(R.id.activity_sekolah_market_share_radiobutton_internet_3);
        RadioButton radioButtonBroadbandOther = findViewById(R.id.activity_sekolah_market_share_radiobutton_internet_other);

        customRadioGroupBroadband = new CustomRadioGroup();
        customRadioGroupBroadband.add(radioButtonBroadbandTelkomsel);
        customRadioGroupBroadband.add(radioButtonBroadbandOoredoo);
        customRadioGroupBroadband.add(radioButtonBroadbandXl);
        customRadioGroupBroadband.add(radioButtonBroadbandAxis);
        customRadioGroupBroadband.add(radioButtonBroadband3);
        customRadioGroupBroadband.add(radioButtonBroadbandOther);

        //msisdn
        editTextMsisdn = findViewById(R.id.activity_sekolah_market_share_edittext_msisdn);
        //amount
        editTextAmount = findViewById(R.id.activity_sekolah_market_share_edittext_rechargementamt);
        //transaksi
        editTextTransaksi = findViewById(R.id.activity_sekolah_market_share_edittext_rechargementtrx);

        //music
        checkBoxMusicSpotify = findViewById(R.id.activity_sekolah_market_share_checkbox_music_spotify);
        checkBoxMusicJoox = findViewById(R.id.activity_sekolah_market_share_checkbox_music_joox);
        checkBoxMusicLangitMusic = findViewById(R.id.activity_sekolah_market_share_checkbox_music_langitmusic);

        //video
        checkBoxVideoViu = findViewById(R.id.activity_sekolah_market_share_checkbox_video_viu);
        checkBoxVideoHooq = findViewById(R.id.activity_sekolah_market_share_checkbox_video_hooq);
        checkBoxVideoNetflix = findViewById(R.id.activity_sekolah_market_share_checkbox_video_netflix);
        checkBoxVideoIflix = findViewById(R.id.activity_sekolah_market_share_checkbox_video_iflix);

        //game
        checkBoxGameMobileLegend = findViewById(R.id.activity_sekolah_market_share_checkbox_game_mobilelegend);
        checkBoxGameAOV = findViewById(R.id.activity_sekolah_market_share_checkbox_game_aov);
        checkBoxGameVainGlory = findViewById(R.id.activity_sekolah_market_share_checkbox_game_vainglory);
        checkBoxGameGetRich = findViewById(R.id.activity_sekolah_market_share_checkbox_game_getrich);
        checkBoxGameCOC = findViewById(R.id.activity_sekolah_market_share_checkbox_game_coc);
        checkBoxGameClashRoyale = findViewById(R.id.activity_sekolah_market_share_checkbox_game_clashroyale);

        checkBoxSosmedFacebook = findViewById(R.id.activity_sekolah_market_share_checkbox_sosmed_facebook);
        checkBoxSosmedInstagram = findViewById(R.id.activity_sekolah_market_share_checkbox_sosmed_instagram);
        checkBoxSosmedPath = findViewById(R.id.activity_sekolah_market_share_checkbox_sosmed_path);
        checkBoxSosmedBBM = findViewById(R.id.activity_sekolah_market_share_checkbox_sosmed_bbm);
        checkBoxSosmedLine = findViewById(R.id.activity_sekolah_market_share_checkbox_sosmed_line);

        //tcash
        radioGroupTcash = findViewById(R.id.activity_sekolah_market_share_radiobox_tcash);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.menu_canteen_edit_action_simpan){
            if(customRadioGroupLegacy.getSelectedItem() == null){
                Toast.makeText(this, "Operator kartu telepon belum diisi, mohon cek kembali", Toast.LENGTH_SHORT).show();
                return super.onOptionsItemSelected(item);
            }

            if(customRadioGroupBroadband.getSelectedItem() == null){
                Toast.makeText(this, "Operator kartu internet belum diisi, mohon cek kembali", Toast.LENGTH_SHORT).show();
                return super.onOptionsItemSelected(item);
            }

            if(editTextMsisdn.getText().toString().equals("")){
                Toast.makeText(this, "MSISDN utama kosong, mohon cek kembali", Toast.LENGTH_SHORT).show();
                return super.onOptionsItemSelected(item);
            }

            if(editTextMsisdn.getText().toString().length() < 11 || editTextMsisdn.getText().toString().length() > 13){
                Toast.makeText(this, "MSISDN tidak dikenali, mohon cek kembali", Toast.LENGTH_SHORT).show();
                return super.onOptionsItemSelected(item);
            }

            if(editTextAmount.getText().toString().equals("")){
                Toast.makeText(this, "Jumlah isi pulsa per bulan kosong, mohon cek kembali", Toast.LENGTH_SHORT).show();
                return super.onOptionsItemSelected(item);
            }

            if(Integer.parseInt(editTextAmount.getText().toString()) > StartupConfig.getSekolahMarketShareAmt()){
                Toast.makeText(this, "Jumlah rupiah setiap transaksi pulsa tidak boleh melebihi " + StartupConfig.getSekolahMarketShareAmt() + ", harap isi data dengan sebenarnya", Toast.LENGTH_SHORT).show();
                return super.onOptionsItemSelected(item);
            }

            if(editTextTransaksi.getText().toString().equals("")){
                Toast.makeText(this, "Jumlah transaksi isi pulsa per bulan kosong, mohon cek kembali", Toast.LENGTH_SHORT).show();
                return super.onOptionsItemSelected(item);
            }

            if(Integer.parseInt(editTextTransaksi.getText().toString()) > StartupConfig.getSekolahMarketShareTrx()){
                Toast.makeText(this, "Jumlah transaksi isi pulsa tidak boleh melebihi " + StartupConfig.getSekolahMarketShareTrx() + ", mohon isi data dengan sebenarnya", Toast.LENGTH_SHORT).show();
                return super.onOptionsItemSelected(item);
            }

            StringBuilder sbMusic = new StringBuilder();
            if(checkBoxMusicSpotify.isChecked()) sbMusic.append("spotify,");
            if(checkBoxMusicJoox.isChecked()) sbMusic.append("joox,");
            if(checkBoxMusicLangitMusic.isChecked()) sbMusic.append("langitmusik,");
            String telkomselDigiMusic = "";
            if(sbMusic.length() > 0) telkomselDigiMusic = sbMusic.substring(0, sbMusic.length() - 1);

            StringBuilder sbVideo = new StringBuilder();
            if(checkBoxVideoViu.isChecked()) sbVideo.append("view,");
            if(checkBoxVideoHooq.isChecked()) sbVideo.append("hooq,");
            if(checkBoxVideoNetflix.isChecked()) sbVideo.append("netflix,");
            if(checkBoxVideoIflix.isChecked()) sbVideo.append("iflix,");
            String telkomselDigiVideo = "";
            if(sbVideo.length() > 0) telkomselDigiVideo = sbVideo.substring(0, sbVideo.length() - 1);

            StringBuilder sbGame = new StringBuilder();
            if(checkBoxGameMobileLegend.isChecked()) sbGame.append("mobilelegend,");
            if(checkBoxGameAOV.isChecked()) sbGame.append("aov,");
            if(checkBoxGameVainGlory.isChecked()) sbGame.append("vainglory,");
            if(checkBoxGameGetRich.isChecked()) sbGame.append("getrich,");
            if(checkBoxGameCOC.isChecked()) sbGame.append("coc,");
            if(checkBoxGameClashRoyale.isChecked()) sbGame.append("clashroyale,");
            String telkomselDigiGame = "";
            if(sbGame.length() > 0) telkomselDigiGame = sbGame.substring(0, sbGame.length() - 1);

            StringBuilder sbSosmed = new StringBuilder();
            if(checkBoxSosmedFacebook.isChecked()) sbSosmed.append("facebook,");
            if(checkBoxSosmedInstagram.isChecked()) sbSosmed.append("instagram,");
            if(checkBoxSosmedPath.isChecked()) sbSosmed.append("path,");
            if(checkBoxSosmedBBM.isChecked()) sbSosmed.append("bbm,");
            if(checkBoxSosmedLine.isChecked()) sbSosmed.append("line,");
            String telkomselDigiSosmed = "";
            if(sbSosmed.length() > 0) telkomselDigiSosmed = sbSosmed.substring(0, sbSosmed.length() - 1);

            boolean telkomselDigiTcash = radioGroupTcash.getCheckedRadioButtonId() == R.id.activity_sekolah_market_share_radiobutton_yes ? true : false;

            ServerHandler.Parameter [] parameters = new ServerHandler.Parameter[]{
                    new ServerHandler.Parameter("npsn", User.instance.getCachedCheckinPlace().getId()),
                    new ServerHandler.Parameter("legacysim", Utility.convertOperatorIdToString(customRadioGroupLegacy.getSelectedItemIdx())),
                    new ServerHandler.Parameter("broadbandsim", Utility.convertOperatorIdToString(customRadioGroupBroadband.getSelectedItemIdx())),
                    new ServerHandler.Parameter("msisdn", Utility.convertPhoneNumberToStandardLocalPhoneNmber(editTextMsisdn.getText().toString())),
                    new ServerHandler.Parameter("rechargeamt", editTextAmount.getText().toString()),
                    new ServerHandler.Parameter("rechargetrx", editTextTransaksi.getText().toString()),
                    new ServerHandler.Parameter("telkomseldigimusic", telkomselDigiMusic),
                    new ServerHandler.Parameter("telkomseldigivideo", telkomselDigiVideo),
                    new ServerHandler.Parameter("telkomseldigigame", telkomselDigiGame),
                    new ServerHandler.Parameter("telkomseldigitcash", String.valueOf(telkomselDigiTcash)),
                    new ServerHandler.Parameter("telkomseldigisocmed", telkomselDigiSosmed),
                    new ServerHandler.Parameter("userid", User.instance.getPhoneOrUserId()),
                    new ServerHandler.Parameter("username", User.instance.getUsername())
            };

            final ProgressDialog progressDialog = new ProgressDialog(SekolahMarketShareActivity.this);
            progressDialog.setMessage("Mengupload market share ke server");
            progressDialog.setCancelable(false);
            progressDialog.show();

            ServerHandler serverHandler = new ServerHandler("sp_institution_marketshare_insert.php", parameters);
            serverHandler.addListener(new ServerHandler.Listener() {
                @Override
                public void onSuccess(String result) {
                    progressDialog.hide();
                    if(result.equals("DataSaved")){
                        Toast.makeText(SekolahMarketShareActivity.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                        finish();
                    }else if(result.equals("InvalidMSISDN")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(SekolahMarketShareActivity.this);
                        builder.setMessage("MSISDN tidak dikenali");
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }else if(result.equals("AlreadySurveyed")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(SekolahMarketShareActivity.this);
                        builder.setMessage("MSISDN sudah pernah di survey");
                        AlertDialog alertDialog = builder.create();
                        alertDialog.show();
                    }else {
                        onError(new Exception(result));
                    }
                }

                @Override
                public void onError(Exception error) {
                    error.printStackTrace();
                    progressDialog.hide();
                    Toast.makeText(SekolahMarketShareActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
            serverHandler.execute();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_canteen_edit, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
