package com.telkomsel.nami.sekolah;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StartupConfig;
import com.telkomsel.nami.universal.core.User;

public class SekolahMarketShareByQuickCountActivity extends AppCompatActivity {
    private EditText editTextTelkomsel, editTextOoredoo, editTextXl, editTextAxis, editText3, editTextOther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sekolah_market_share_by_quick_count);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editTextTelkomsel = (EditText)findViewById(R.id.activity_sekolah_market_share_by_quick_count_edittext_telkomsel);
        editTextOoredoo = (EditText)findViewById(R.id.activity_sekolah_market_share_by_quick_count_edittext_ooredoo);
        editTextXl = (EditText)findViewById(R.id.activity_sekolah_market_share_by_quick_count_edittext_xl);
        editTextAxis = (EditText)findViewById(R.id.activity_sekolah_market_share_by_quick_count_edittext_axis);
        editText3 = (EditText)findViewById(R.id.activity_sekolah_market_share_by_quick_count_edittext_3);
        editTextOther = (EditText)findViewById(R.id.activity_sekolah_market_share_by_quick_count_edittext_other);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_canteen_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.menu_canteen_edit_action_simpan){
            boolean isError = false;

            EditText [] editTexts = {editTextTelkomsel, editTextOoredoo, editTextXl, editTextAxis, editText3, editTextOther};
            for (EditText editText : editTexts) {
                String text = editText.getText().toString();
                if(text.equals("")){
                    Toast.makeText(SekolahMarketShareByQuickCountActivity.this, "Satu atau lebih isian belum diisi, mohon cek kembali", Toast.LENGTH_SHORT).show();
                    isError = true;
                    break;
                }else if(Integer.parseInt(text) > StartupConfig.getSekolahMarketShareQuickCountAmt()){
                    Toast.makeText(SekolahMarketShareByQuickCountActivity.this, "Isian Quick Count tidak boleh lebih dari " + StartupConfig.getSekolahMarketShareQuickCountAmt() + ", mohon isi dengan sebenarnya", Toast.LENGTH_SHORT).show();
                    isError = true;
                    break;
                }
            }

            if(!isError){
                ServerHandler.Parameter [] parameters = new ServerHandler.Parameter[]{
                    new ServerHandler.Parameter("npsn", User.instance.getCachedCheckinPlace().getId()),
                    new ServerHandler.Parameter("usertelkomsel", editTextTelkomsel.getText().toString()),
                    new ServerHandler.Parameter("userooredoo", editTextOoredoo.getText().toString()),
                    new ServerHandler.Parameter("userxl", editTextXl.getText().toString()),
                    new ServerHandler.Parameter("useraxis", editTextAxis.getText().toString()),
                    new ServerHandler.Parameter("user3", editText3.getText().toString()),
                    new ServerHandler.Parameter("userothers", editTextOther.getText().toString()),
                    new ServerHandler.Parameter("userid", User.instance.getPhoneOrUserId()),
                    new ServerHandler.Parameter("username", User.instance.getUsername())
                };

                final ProgressDialog progressDialog = new ProgressDialog(SekolahMarketShareByQuickCountActivity.this);
                progressDialog.setMessage("Mengupload market share ke server");
                progressDialog.setCancelable(false);
                progressDialog.show();

                ServerHandler serverHandler = new ServerHandler("sp_institution_marketshare_quickcount_insert.php", parameters);
                serverHandler.addListener(new ServerHandler.Listener() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("true")){
                            progressDialog.hide();
                            Toast.makeText(SekolahMarketShareByQuickCountActivity.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                            finish();
                        }else {
                            onError(new Exception(result));
                        }
                    }

                    @Override
                    public void onError(Exception error) {
                        Log.e("Nami", error.toString());
                        Toast.makeText(SekolahMarketShareByQuickCountActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                });
                serverHandler.execute();
            }
        }
        return onContextItemSelected(item);
    }
}
