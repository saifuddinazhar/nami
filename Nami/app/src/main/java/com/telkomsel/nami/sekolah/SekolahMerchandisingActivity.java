package com.telkomsel.nami.sekolah;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.telkomsel.nami.LoginActivity;
import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.Sekolah;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StartupConfig;
import com.telkomsel.nami.universal.core.User;

public class SekolahMerchandisingActivity extends AppCompatActivity {
    private EditText editTextPoster, editTextBanner, editTextShopsign, editTextTableCloth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sekolah_merchandising);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        editTextPoster = findViewById(R.id.activity_sekolah_merchandising_edittext_poster);
        editTextBanner = findViewById(R.id.activity_sekolah_merchandising_edittext_banner);
        editTextShopsign = findViewById(R.id.activity_sekolah_merchandising_edittext_shopsign);
        editTextTableCloth = findViewById(R.id.activity_sekolah_merchandising_edittext_tablecloth);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_barcode_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.menu_barcode_list_action_upload){
            boolean isValidationPass = true;

            String [] inputValues = new String[] {editTextPoster.getText().toString(), editTextBanner.getText().toString(),
                    editTextBanner.getText().toString(), editTextShopsign.getText().toString(), editTextTableCloth.getText().toString()};

            for (String inputValue: inputValues) {
                if(inputValue == null || inputValue.equals("")){
                    Toast.makeText(SekolahMerchandisingActivity.this, "Data kurang lengkap, mohon cek kembali", Toast.LENGTH_SHORT).show();
                    isValidationPass = false;
                    break;
                }
            }

            if(isValidationPass){
                if(Integer.parseInt(editTextBanner.getText().toString()) > StartupConfig.getSekolahMerchandisingBanner()){
                    Toast.makeText(SekolahMerchandisingActivity.this, "Banner tidak boleh lebih dari " + StartupConfig.getSekolahMerchandisingBanner() + ", harap isi dengan sebenarnya", Toast.LENGTH_SHORT).show();
                    isValidationPass = false;
                }
            }

            if(isValidationPass){
                if(Integer.parseInt(editTextPoster.getText().toString()) > StartupConfig.getSekolahMerchandisingPoster()){
                    Toast.makeText(SekolahMerchandisingActivity.this, "Poster tidak boleh lebih dari " + StartupConfig.getSekolahMerchandisingPoster() + ", harap isi dengan sebenarnya", Toast.LENGTH_SHORT).show();
                    isValidationPass = false;
                }
            }

            if(isValidationPass){
                if(Integer.parseInt(editTextShopsign.getText().toString()) > StartupConfig.getSekolahMerchandisingShopsign()){
                    Toast.makeText(SekolahMerchandisingActivity.this, "Shopsign tidak boleh lebih dari " + StartupConfig.getSekolahMerchandisingShopsign() + ", harap isi dengan sebenarnya", Toast.LENGTH_SHORT).show();
                    isValidationPass = false;
                }
            }

            if(isValidationPass){
                if(Integer.parseInt(editTextTableCloth.getText().toString()) > StartupConfig.getSekolahMerchandisingTableCloth()){
                    Toast.makeText(SekolahMerchandisingActivity.this, "Taplak meja tidak boleh lebih dari " + StartupConfig.getSekolahMerchandisingTableCloth() + ", harap isi dengan sebenarnya", Toast.LENGTH_SHORT).show();
                    isValidationPass = false;
                }
            }

            if(isValidationPass){
                final ProgressDialog progressDialog = new ProgressDialog(SekolahMerchandisingActivity.this);
                progressDialog.setMessage("Menyimpan data merchandising");
                progressDialog.setCancelable(false);
                progressDialog.show();

                ServerHandler.Parameter [] parameters = new ServerHandler.Parameter[]{
                        new ServerHandler.Parameter("npsn", User.instance.getCachedCheckinPlace().getId()),
                        new ServerHandler.Parameter("poster", editTextPoster.getText().toString()),
                        new ServerHandler.Parameter("banner", editTextBanner.getText().toString()),
                        new ServerHandler.Parameter("shopsign", editTextShopsign.getText().toString()),
                        new ServerHandler.Parameter("tablecloth", editTextTableCloth.getText().toString()),
                        new ServerHandler.Parameter("userid", User.instance.getPhoneOrUserId()),
                        new ServerHandler.Parameter("username", User.instance.getUsername()),
                };

                ServerHandler serverHandler = new ServerHandler("sp_institution_merchandising_insert.php", parameters);
                serverHandler.addListener(new ServerHandler.Listener() {
                    @Override
                    public void onSuccess(String result) {
                        if(result.equals("true")){
                            progressDialog.hide();
                            Toast.makeText(SekolahMerchandisingActivity.this, "Data berhasil disimpan", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            onError(new Exception(result));
                        }
                    }

                    @Override
                    public void onError(Exception error) {
                        Toast.makeText(SekolahMerchandisingActivity.this, error.toString(), Toast.LENGTH_SHORT).show();
                        error.printStackTrace();
                    }
                });

                serverHandler.execute();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
