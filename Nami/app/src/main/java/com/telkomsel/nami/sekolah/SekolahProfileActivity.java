package com.telkomsel.nami.sekolah;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.telkomsel.nami.R;
import com.telkomsel.nami.ossosk.OssOskSurveyActivity;
import com.telkomsel.nami.ossosk.fragment.OssOskSurveyFragment;
import com.telkomsel.nami.sekolah.fragment.SekolahProfileADOFragment;
import com.telkomsel.nami.sekolah.fragment.SekolahProfileAmbassadorFragment;
import com.telkomsel.nami.sekolah.fragment.SekolahProfileBrandingFragment;
import com.telkomsel.nami.sekolah.fragment.SekolahProfileChairmanFragment;
import com.telkomsel.nami.sekolah.fragment.SekolahProfileFragment;
import com.telkomsel.nami.sekolah.fragment.SekolahProfileInfluencerFragment;
import com.telkomsel.nami.sekolah.fragment.SekolahProfileKualitasSinyalFragment;
import com.telkomsel.nami.sekolah.fragment.SekolahProfilePrincipalFragment;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SekolahProfileActivity extends AppCompatActivity {
    private SekolahProfilePrincipalFragment principalFragment;
    private SekolahProfileInfluencerFragment influencerFragment;
    private SekolahProfileChairmanFragment chairmanFragment;
    private SekolahProfileAmbassadorFragment ambassadorFragment;
    private SekolahProfileADOFragment adoFragment;
    private SekolahProfileKualitasSinyalFragment kualitasSinyalFragment;
    private SekolahProfileBrandingFragment brandingFragment;

    private boolean isInsert = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sekolah_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.activity_sekolah_profile_viewpager);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.activity_sekolah_profile_tablayout);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(10);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        principalFragment = new SekolahProfilePrincipalFragment();
        influencerFragment = new SekolahProfileInfluencerFragment();
        chairmanFragment = new SekolahProfileChairmanFragment();
        ambassadorFragment = new SekolahProfileAmbassadorFragment();
        adoFragment = new SekolahProfileADOFragment();
        kualitasSinyalFragment = new SekolahProfileKualitasSinyalFragment();
        brandingFragment = new SekolahProfileBrandingFragment();

        adapter.addFragment(principalFragment, "REKTOR/KEPSEK");
        adapter.addFragment(influencerFragment, "WATOR/WAKASEK");
        adapter.addFragment(chairmanFragment, "OSIS/BEM/HMJ");
        adapter.addFragment(ambassadorFragment, "AMBASSADOR");
        adapter.addFragment(adoFragment, "PROFILE");
        adapter.addFragment(kualitasSinyalFragment, "KUALITAS SINYAL");
        adapter.addFragment(brandingFragment, "BRANDING");

        viewPager.setAdapter(adapter);

        final ProgressDialog progressDialog = new ProgressDialog(SekolahProfileActivity.this);
        progressDialog.setMessage("Memuat data profile sekolah");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ServerHandler.Parameter [] parameters = {
                new ServerHandler.Parameter("npsn", User.instance.getCachedCheckinPlace().getId())
        };
        ServerHandler serverHandler = new ServerHandler("sp_institution_profiling_read.php", parameters);
        serverHandler.addListener(new ServerHandler.Listener() {
            @Override
            public void onSuccess(String result) {
                progressDialog.hide();
                if(!result.equals("norecord")){
                    try {
                        isInsert = false;

                        JSONObject jsonProfile = new JSONObject(result);
                        principalFragment.setProfileData(jsonProfile);
                        influencerFragment.setProfileData(jsonProfile);
                        chairmanFragment.setProfileData(jsonProfile);
                        ambassadorFragment.setProfileData(jsonProfile);
                        adoFragment.setProfileData(jsonProfile);
                        kualitasSinyalFragment.setProfileData(jsonProfile);
                        brandingFragment.setProfileData(jsonProfile);
                    } catch (JSONException e) {
                        Log.e("Nami", e.toString());
                    }
                }
            }

            @Override
            public void onError(Exception error) {
                error.printStackTrace();
            }
        });
        serverHandler.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_barcode_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.menu_barcode_list_action_upload){
            SekolahProfileFragment[] fragments = {principalFragment, influencerFragment, chairmanFragment,
                    ambassadorFragment, adoFragment, kualitasSinyalFragment, brandingFragment};

            ArrayList<ServerHandler.Parameter> parameters = new ArrayList<>();

            for (SekolahProfileFragment fragment : fragments) {
                String validationMessage = fragment.getValidationMessage();
                if(validationMessage != null){
                    showValidationMessageWarning(validationMessage);
                    return super.onOptionsItemSelected(item);
                }

                ServerHandler.Parameter [] fragmentParameters = fragment.getServerHandlerParameters();
                for (ServerHandler.Parameter fragmentParameter:fragmentParameters) {
                    parameters.add(fragmentParameter);
                }
            }

            parameters.add(new ServerHandler.Parameter("userid", User.instance.getPhoneOrUserId()));
            parameters.add(new ServerHandler.Parameter("username",User.instance.getUsername()));
            parameters.add(new ServerHandler.Parameter("npsn", User.instance.getCachedCheckinPlace().getId()));
            parameters.add(new ServerHandler.Parameter("modes", isInsert ? "insert" : "update"));

            final ProgressDialog progressDialog = new ProgressDialog(SekolahProfileActivity.this);
            progressDialog.setMessage("Mengupload hasil survey ke server");
            progressDialog.setCancelable(false);
            progressDialog.show();

            ServerHandler serverHandler = new ServerHandler("sp_institution_profiling_insert.php", parameters.toArray(new ServerHandler.Parameter[parameters.size()]));
            serverHandler.addListener(new ServerHandler.Listener() {
                @Override
                public void onSuccess(String result) {
                    progressDialog.hide();
                    if(result.equals("true")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(SekolahProfileActivity.this);
                        builder.setMessage("Survey telah berhasil di upload, terima kasih").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        }).show();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SekolahProfileActivity.this);
                        builder.setMessage("Terjadi kesalahan : " + result).setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        }).show();
                    }
                }

                @Override
                public void onError(Exception error) {
                    error.printStackTrace();
                }
            });
            serverHandler.execute();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void showValidationMessageWarning(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message).setPositiveButton("Ok",null).show();
    }

    //Inner class
    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public int getItemPosition(Object object){
            return super.getItemPosition(object);

        }
    }
}
