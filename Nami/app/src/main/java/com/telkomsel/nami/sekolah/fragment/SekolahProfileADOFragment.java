package com.telkomsel.nami.sekolah.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by saifu on 2/26/2018.
 */

public class SekolahProfileADOFragment extends SekolahProfileFragment  {
    private EditText  editTextKegiatan, editTextUltah, editTextJumlahGuru, editTextJumlahSiswa;
    private EditText editTextJumlahLab, editTextJumlahSportRoom, editTextJumlahKelas, editTextJumlahKoperasi, editTextJumlahKantin;
    private EditText editTextFacebook, editTextInstagram, editTextWeb;

    @Override
    public void setProfileData(JSONObject jsonProfile) {
        super.setProfileData(jsonProfile);
        try {
            editTextKegiatan.setText(jsonProfile.getString("interested_activities"));
            editTextUltah.setText(Utility.convertDateMySqlStringToIndonesianString(jsonProfile.getString("institution_birthday")));
            editTextJumlahGuru.setText(jsonProfile.getString("total_teacher"));
            editTextJumlahSiswa.setText(jsonProfile.getString("total_students"));
            editTextJumlahLab.setText(jsonProfile.getString("total_laboratorium"));
            editTextJumlahSportRoom.setText(jsonProfile.getString("total_sportroom"));
            editTextJumlahKelas.setText(jsonProfile.getString("total_classroom"));
            editTextJumlahKoperasi.setText(jsonProfile.getString("total_cooperative"));
            editTextJumlahKantin.setText(jsonProfile.getString("total_canteen"));
            editTextFacebook.setText(jsonProfile.getString("institution_fb"));
            editTextInstagram.setText(jsonProfile.getString("institution_instagram"));
            editTextWeb.setText(jsonProfile.getString("institution_url"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sekolah_profile_ado, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d("Nami", "On view created ADO");

        editTextUltah = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_ado_birthday);
        editTextKegiatan = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_ado_interestedactivity);
        editTextJumlahGuru = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_ado_jumlahguru);
        editTextJumlahSiswa = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_ado_jumlahsiswa);
        editTextJumlahLab = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_ado_jumlahlab);
        editTextJumlahSportRoom = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_ado_jumlahsportroom);
        editTextJumlahKelas = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_ado_jumlahkelas);
        editTextJumlahKoperasi = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_ado_jumlahkoperasi);
        editTextJumlahKantin = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_ado_jumlahkantin);
        editTextFacebook = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_ado_facebook);
        editTextInstagram = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_ado_instagram);
        editTextWeb = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_ado_web);
        editTextUltah.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean isFocus) {
                if(isFocus){
                    Time today = new Time(Time.getCurrentTimezone());
                    today.setToNow();

                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                            editTextUltah.setText(Utility.convertDateToIndonesianString(d, m+1, y));
                        }
                    }, today.year, today.month, today.monthDay);
                    datePickerDialog.show();
                }
            }
        });
    }

    @Override
    public String getValidationMessage() {
        String [] strings = {editTextUltah.getText().toString(), editTextKegiatan.getText().toString(),
                editTextJumlahSiswa.getText().toString(), editTextJumlahLab.getText().toString(),
                editTextJumlahSportRoom.getText().toString(), editTextJumlahKelas.getText().toString(),
                editTextJumlahKoperasi.getText().toString(), editTextJumlahKantin.getText().toString()
        };
        for (String string:strings) {
            if(string == null || string.equals("")){
                return "Salah satu isian atau lebih pada tab PROFILE masih kosong, silahkan lengkapi terlebih dahulu";
            }
        }
        return  null;
    }

    @Override
    public ServerHandler.Parameter[] getServerHandlerParameters() {
        ServerHandler.Parameter [] parameters = {
                new ServerHandler.Parameter("institutionbirthday", Utility.convertDateIndonesianStringToMysqlString(editTextUltah.getText().toString())),
                new ServerHandler.Parameter("interestedactivities", editTextKegiatan.getText().toString()),
                new ServerHandler.Parameter("totalteacher", editTextJumlahGuru.getText().toString()),
                new ServerHandler.Parameter("totalstudents", editTextJumlahSiswa.getText().toString()),
                new ServerHandler.Parameter("totallaboratorium", editTextJumlahLab.getText().toString()),
                new ServerHandler.Parameter("totalsportroom", editTextJumlahSportRoom.getText().toString()),
                new ServerHandler.Parameter("totalclassroom", editTextJumlahKelas.getText().toString()),
                new ServerHandler.Parameter("totalcooperative", editTextJumlahKoperasi.getText().toString()),
                new ServerHandler.Parameter("institutionfb", editTextFacebook.getText().toString()),
                new ServerHandler.Parameter("institutioninstagram", editTextInstagram.getText().toString()),
                new ServerHandler.Parameter("institutionurl", editTextWeb.getText().toString()),
                new ServerHandler.Parameter("totalcanteen", editTextJumlahKantin.getText().toString())
        };
        return parameters;
    }
}
