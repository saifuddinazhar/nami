package com.telkomsel.nami.sekolah.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.ServerHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by saifu on 2/27/2018.
 */

public class SekolahProfileBrandingFragment extends SekolahProfileFragment {
    private Spinner spinnerLapangan, spinnerTembok, spinnerMading;

    private JSONObject jsonProfile;

    @Override
    public void setProfileData(JSONObject jsonProfile) {
        super.setProfileData(jsonProfile);
        this.jsonProfile = jsonProfile;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sekolah_profile_branding, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d("Nami", "On view created Branding");

        spinnerLapangan = (Spinner) getView().findViewById(R.id.fragment_sekolah_profile_branding_spinner_lapangan);
        spinnerTembok = (Spinner) getView().findViewById(R.id.fragment_sekolah_profile_branding_spinner_tembok);
        spinnerMading = (Spinner) getView().findViewById(R.id.fragment_sekolah_profile_branding_spinner_mading);

        ServerHandler serverHandler = new ServerHandler("sp_config_branding.php", new ServerHandler.Parameter[0]);
        serverHandler.addListener(new ServerHandler.Listener() {
            @Override
            public void onSuccess(String result) {
                try {
                    JSONArray jsonDominants = new JSONArray(result);
                    final String [] dominants = new String[jsonDominants.length()];

                    for(int i=0; i<jsonDominants.length(); i++){
                        dominants[i] = jsonDominants.getString(i);
                    }

                    ArrayAdapter adapterDominantBranding = new ArrayAdapter<CharSequence>(getActivity(), android.R.layout.simple_spinner_item);
                    adapterDominantBranding.add("--Pilih--");
                    for (String dominantBrand: dominants) {
                        adapterDominantBranding.add(dominantBrand);
                    }
                    spinnerLapangan.setAdapter(adapterDominantBranding);
                    spinnerTembok.setAdapter(adapterDominantBranding);
                    spinnerMading.setAdapter(adapterDominantBranding);

                    if(jsonProfile != null){
                        String branding = jsonProfile.getString("field_branding");
                        setSpinnerSelected(branding, spinnerLapangan);

                        branding = jsonProfile.getString("wall_branding");
                        setSpinnerSelected(branding, spinnerTembok);

                        branding = jsonProfile.getString("wallmagazine_branding");
                        setSpinnerSelected(branding, spinnerMading);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(Exception error) {
                error.printStackTrace();
            }
        });
        serverHandler.execute();
    }

    private void setSpinnerSelected(String brandingString, Spinner spinner){
        for(int i=0; i<spinner.getAdapter().getCount(); i++){
            String spinnerItemString =  (String) spinner.getAdapter().getItem(i);
            if(spinnerItemString.equals(brandingString)){
                spinner.setSelection(i);
                break;
            }
        }
    }

    @Override
    public String getValidationMessage() {
        String [] strings = {spinnerLapangan.getSelectedItem().toString(), spinnerTembok.getSelectedItem().toString(),
                spinnerMading.getSelectedItem().toString()
        };
        for (String string:strings) {
            if(string == null || string.equals("--Pilih--")){
                return "Salah satu isian atau lebih pada tab BRANDING masih kosong, silahkan lengkapi terlebih dahulu";
            }
        }
        return  null;
    }

    @Override
    public ServerHandler.Parameter[] getServerHandlerParameters() {
        ServerHandler.Parameter [] parameters = {
                new ServerHandler.Parameter("fieldbranding", spinnerLapangan.getSelectedItem().toString()),
                new ServerHandler.Parameter("wallbranding", spinnerTembok.getSelectedItem().toString()),
                new ServerHandler.Parameter("wallmagazinebranding", spinnerMading.getSelectedItem().toString())
        };
        return parameters;
    }
}
