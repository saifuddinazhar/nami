package com.telkomsel.nami.sekolah.fragment;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StartupConfig;
import com.telkomsel.nami.universal.core.Utility;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by saifu on 2/26/2018.
 */

public class SekolahProfileChairmanFragment extends SekolahProfileFragment  {
    private EditText editTextNama, editTextKontak, editTextUltah, editTextHobi, editTextAgama;

    @Override
    public void setProfileData(JSONObject jsonProfile) {
        super.setProfileData(jsonProfile);

        try {
            editTextNama.setText(jsonProfile.getString("chairman_name"));
            editTextKontak.setText(jsonProfile.getString("chairman_contact"));
            editTextUltah.setText(Utility.convertDateMySqlStringToIndonesianString(jsonProfile.getString("chairman_birthday")));
            editTextHobi.setText(jsonProfile.getString("chairman_hobby"));
            editTextAgama.setText(jsonProfile.getString("chairman_religion"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sekolah_profile_chairman, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d("Nami", "On view created Chairman");

        editTextNama = (EditText) getView().findViewById(R.id.fragment_sekolah_profile_chairman_nama);
        editTextKontak = (EditText) getView().findViewById(R.id.fragment_sekolah_profile_chairman_kontak);
        editTextUltah = (EditText) getView().findViewById(R.id.fragment_sekolah_profile_chairman_ultah);
        editTextHobi = (EditText) getView().findViewById(R.id.fragment_sekolah_profile_chairman_hobbi);
        editTextAgama = (EditText) getView().findViewById(R.id.fragment_sekolah_profile_chairman_agama);

        editTextUltah.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean isFocus) {
                if(isFocus){
                    Time today = new Time(Time.getCurrentTimezone());
                    today.setToNow();

                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                            editTextUltah.setText(Utility.convertDateToIndonesianString(d, m+1, y));
                        }
                    }, today.year, today.month, today.monthDay);
                    datePickerDialog.show();
                }
            }
        });

        editTextAgama.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View view, boolean isFocus) {
                if(isFocus){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Pilih agama");
                    builder.setItems(StartupConfig.getReligions(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ((EditText)view).setText(StartupConfig.getReligions()[i]);
                        }
                    });
                    builder.create().show();
                }
            }
        });
    }

    @Override
    public String getValidationMessage() {
        String [] strings = {editTextNama.getText().toString(), editTextKontak.getText().toString(),
                editTextUltah.getText().toString(), editTextHobi.getText().toString(), editTextAgama.getText().toString()};
        for (String string:strings) {
            if(string == null || string.equals("")){
                return "Salah satu isian atau lebih pada tab OSIS/BEM/HMJ masih kosong, silahkan lengkapi terlebih dahulu";
            }
        }
        return  null;
    }

    @Override
    public ServerHandler.Parameter[] getServerHandlerParameters() {
        ServerHandler.Parameter [] parameters = {
                new ServerHandler.Parameter("chairmanname", editTextNama.getText().toString()),
                new ServerHandler.Parameter("chairmancontact", editTextKontak.getText().toString()),
                new ServerHandler.Parameter("chairmanbirthday", Utility.convertDateIndonesianStringToMysqlString(editTextUltah.getText().toString())),
                new ServerHandler.Parameter("chairmanhobby", editTextHobi.getText().toString()),
                new ServerHandler.Parameter("chairmanreligion", editTextAgama.getText().toString())
        };
        return parameters;
    }
}
