package com.telkomsel.nami.sekolah.fragment;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.format.Time;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.telkomsel.nami.R;
import com.telkomsel.nami.sekolah.SekolahProfileActivity;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StartupConfig;
import com.telkomsel.nami.universal.core.Utility;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by saifu on 2/19/2018.
 */

public class SekolahProfilePrincipalFragment extends SekolahProfileFragment {
    EditText editTextNama, editTextKontak, editTextUlangTahun, editTextHobi, editTextAgama;

    @Override
    public void setProfileData(JSONObject jsonProfile) {
        super.setProfileData(jsonProfile);
        try {
            editTextNama.setText(jsonProfile.getString("principal_name"));
            editTextKontak.setText(jsonProfile.getString("principal_contact"));
            editTextUlangTahun.setText(Utility.convertDateMySqlStringToIndonesianString(jsonProfile.getString("principal_birthday")));
            editTextHobi.setText(jsonProfile.getString("principal_hobby"));
            editTextAgama.setText(jsonProfile.getString("principal_religion"));

        } catch (JSONException e) {
            Log.e("Nami", e.toString());
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sekolah_profile_principal, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d("Nami", "On view created Principal");

        editTextNama = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_principal_nama);
        editTextKontak = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_principal_kontak);
        editTextUlangTahun = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_principal_ultah);
        editTextHobi = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_principal_hobi);
        editTextAgama = (EditText)getView().findViewById(R.id.fragment_sekolah_profile_principal_agama);

        editTextUlangTahun.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean isFocus) {
                if(isFocus){
                    Time today = new Time(Time.getCurrentTimezone());
                    today.setToNow();

                    DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker datePicker, int y, int m, int d) {
                            editTextUlangTahun.setText(Utility.convertDateToIndonesianString(d, m+1, y));
                        }
                    }, today.year, today.month, today.monthDay);
                    datePickerDialog.show();
                }
            }
        });
        editTextAgama.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(final View view, boolean isFocus) {
                if(isFocus){
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle("Pilih agama");
                    builder.setItems(StartupConfig.getReligions(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            ((EditText)view).setText(StartupConfig.getReligions()[i]);
                        }
                    });
                    builder.create().show();
                }
            }
        });
    }

    @Override
    public String getValidationMessage() {
        String [] strings = {editTextNama.getText().toString(), editTextKontak.getText().toString(),
                editTextUlangTahun.getText().toString(), editTextHobi.getText().toString(), editTextAgama.getText().toString()};
        for (String string:strings) {
            if(string == null || string.equals("")){
                return "Salah satu isian atau lebih pada tab REKTOR/KEPALA SEKOLAH masih kosong, silahkan lengkapi terlebih dahulu";
            }
        }
        return  null;
    }

    @Override
    public ServerHandler.Parameter[] getServerHandlerParameters() {
        ServerHandler.Parameter [] parameters = {
            new ServerHandler.Parameter("principalname", editTextNama.getText().toString()),
            new ServerHandler.Parameter("principalcontact", editTextKontak.getText().toString()),
            new ServerHandler.Parameter("principalbirthday", Utility.convertDateIndonesianStringToMysqlString(editTextUlangTahun.getText().toString())),
            new ServerHandler.Parameter("principalhobby", editTextHobi.getText().toString()),
            new ServerHandler.Parameter("principalreligion", editTextAgama.getText().toString())
        };
        return parameters;
    }
}
