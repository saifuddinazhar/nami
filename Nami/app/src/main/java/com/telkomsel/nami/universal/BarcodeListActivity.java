package com.telkomsel.nami.universal;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.Configuration;
import com.telkomsel.nami.universal.core.OssOsk;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StaticVariable;
import com.telkomsel.nami.universal.core.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BarcodeListActivity extends AppCompatActivity {
    private ListView listView;
    private ArrayList<String> scannedStrings = new ArrayList<>();
    private String channelType, broadbandType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        channelType = getIntent().getStringExtra("channelType");
        broadbandType = getIntent().getStringExtra("broadbandType");

        Button buttonScanBarcode = (Button)findViewById(R.id.activity_barcode_list_button_scan_barcode);
        listView = (ListView)findViewById(R.id.activity_barcode_list_listview);

        buttonScanBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Intent intent = new Intent(BarcodeListActivity.this, BarcodeScanActivity.class);
            startActivityForResult(intent, 0);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_barcode_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.menu_barcode_list_action_upload){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Apakah Anda yakin akan mengupload semua hasil scan barcode ini?").setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    final String [] scannedStrings = BarcodeListActivity.this.scannedStrings.toArray(new String[BarcodeListActivity.this.scannedStrings.size()]);

                    final ProgressDialog progressDialog = new ProgressDialog(BarcodeListActivity.this);
                    progressDialog.setMessage("Mohon tunggu, sedang melakukan proses upload");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    String channelTypeId = (String)StaticVariable.get("sp_sales_batch_insert__channeltypeid", "");
                    JSONArray msisdns = new JSONArray();

                    for (final String scannedString : scannedStrings) {
                        msisdns.put(scannedString);
                    }

                    ServerHandler.Parameter [] parameters =  {
                            new ServerHandler.Parameter("placeId", User.instance.getCachedCheckinPlace().getId()),
                            new ServerHandler.Parameter("msisdns", msisdns.toString()),
                            new ServerHandler.Parameter("channelType", channelType),
                            new ServerHandler.Parameter("channelTypeId", channelTypeId),
                            new ServerHandler.Parameter("broadbandType", broadbandType),
                            new ServerHandler.Parameter("userId", User.instance.getPhoneOrUserId()),
                            new ServerHandler.Parameter("userName", User.instance.getUsername()),
                    };

                    ServerHandler serverHandler = new ServerHandler("sp_sales_batch_insert.php", parameters);
                    serverHandler.addListener(new ServerHandler.Listener() {
                        @Override
                        public void onSuccess(String result) {
                            progressDialog.dismiss();
                            try {
                                new JSONArray(result);

                                Toast.makeText(BarcodeListActivity.this, "Semua data telah terupload", Toast.LENGTH_SHORT).show();
                                BarcodeListActivity.this.scannedStrings.clear();
                                String [] stringToDisplays = BarcodeListActivity.this.scannedStrings.toArray(new String[BarcodeListActivity.this.scannedStrings.size()]);
                                ArrayAdapter arrayAdapter = new ArrayAdapter(BarcodeListActivity.this, android.R.layout.simple_list_item_1, stringToDisplays);
                                listView.setAdapter(arrayAdapter);
                                finish();
                            } catch (JSONException e) {
                                Toast.makeText(BarcodeListActivity.this, "Upload barcode failed : " + result, Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(Exception error) {
                            error.printStackTrace();
                        }
                    });
                    serverHandler.execute();
                }
            }).setNegativeButton("Tidak", null).show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 0 && resultCode == Activity.RESULT_OK){
            String [] scannedStrings = data.getStringArrayExtra("scannedStrings");
            for (String scannedString:scannedStrings) {
                this.scannedStrings.add(scannedString);
            }

            String [] stringToDisplays = this.scannedStrings.toArray(new String[this.scannedStrings.size()]);
            ArrayAdapter arrayAdapter = new ArrayAdapter(BarcodeListActivity.this, android.R.layout.simple_list_item_1, stringToDisplays);
            listView.setAdapter(arrayAdapter);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
