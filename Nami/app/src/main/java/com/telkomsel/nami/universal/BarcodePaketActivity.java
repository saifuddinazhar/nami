package com.telkomsel.nami.universal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StaticVariable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BarcodePaketActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_paket);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final String channelType = getIntent().getStringExtra("channelType");

        final ListView listView = (ListView)findViewById(R.id.activity_pilih_paket_listview);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mendapatkan data paket broadband");
        progressDialog.setCancelable(false);
        progressDialog.show();

        ServerHandler.Parameter [] parameters =  {};
        ServerHandler serverHandler = new ServerHandler("sp_config_broadband.php", parameters);
        serverHandler.addListener(new ServerHandler.Listener() {
            @Override
            public void onSuccess(String result) {
                progressDialog.hide();
                if(result.equals("false")){
                    Toast.makeText(BarcodePaketActivity.this, "Terjadi kesalahan, kontak Administrator Anda", Toast.LENGTH_SHORT).show();
                }else {
                    try {
                        JSONArray jsonPakets = new JSONArray(result);
                        final String [] paketNames = new String[jsonPakets.length()];
                        for(int i=0; i<jsonPakets.length(); i++){
                            JSONObject jsonPaket = jsonPakets.getJSONObject(i);
                            paketNames[i] = jsonPaket.getString("main");
                        }
                        ArrayAdapter arrayAdapter = new ArrayAdapter(BarcodePaketActivity.this, android.R.layout.simple_list_item_1, paketNames);
                        listView.setAdapter(arrayAdapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                Intent intent = new Intent(BarcodePaketActivity.this, BarcodeListActivity.class);
                                intent.putExtra("channelType", channelType);
                                intent.putExtra("broadbandType", paketNames[i]);
                                startActivity(intent);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onError(Exception error) {

            }
        });
        serverHandler.execute();
    }

    @Override
    public boolean onSupportNavigateUp() {
        StaticVariable.remove("sp_sales_batch_insert__channeltypeid");
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        StaticVariable.remove("sp_sales_batch_insert__channeltypeid");
        finish();
    }
}
