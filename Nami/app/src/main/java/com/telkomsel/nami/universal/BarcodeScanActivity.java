package com.telkomsel.nami.universal;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.Result;
import com.telkomsel.nami.R;
import com.telkomsel.nami.ossosk.fragment.OssOskSurveyBrandingFragment;

import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.core.ViewFinderView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class BarcodeScanActivity extends AppCompatActivity {
    private ZXingScannerView mScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_scan);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final TextView textViewLastScan = (TextView)findViewById(R.id.activity_barcode_scan_textview_last_scan);
        final TextView textViewTotalScan = (TextView)findViewById(R.id.activity_barcode_scan_textview_jumlah_scan);
        final Button buttonSelesai = (Button)findViewById(R.id.activity_barcode_scan_button_selesai);
        final ViewGroup contentFrame = (ViewGroup) findViewById(R.id.barcode_scanner);
        final ArrayList<String> scannedStrings = new ArrayList<>();

        mScannerView = new ZXingScannerView(this) ;
        contentFrame.addView(mScannerView);

        mScannerView.setResultHandler(new ZXingScannerView.ResultHandler() {
            @Override
            public void handleResult(Result barcode) {
                String value = barcode.getText();

                try{
                    value = value.substring(value.length() - 11, value.length());
                    if(value.charAt(0) != '8'){
                        value =  value.substring(value.length() - 10, value.length());
                    }

                    value = "62" + value;
                    if(!scannedStrings.contains(value)){
                        scannedStrings.add(value);
                        final String finalValue = value;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                textViewLastScan.setText("Last scan : " + finalValue);
                                textViewTotalScan.setText("Total scan : " + scannedStrings.size());
                            }
                        });
                    }
                }catch (Exception e){
                    textViewLastScan.setText("Tidak dapat mendapatkan MSISDN");
                }

                mScannerView.resumeCameraPreview(this);
            }
        });

        buttonSelesai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("scannedStrings", scannedStrings.toArray(new String[scannedStrings.size()]));
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        } );
    }

    @Override
    public void onResume() {
        super.onResume();
        if ( ContextCompat.checkSelfPermission( this, Manifest.permission.CAMERA ) == PackageManager.PERMISSION_GRANTED ) {
            mScannerView.startCamera();
        } else {
            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setTitle("Peringatan");
            alertDialog.setMessage("Camera Permission dibutuhkan untuk melakukan scan barcode");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if(Build.VERSION.SDK_INT >= 23){
                        requestPermissions(new String[] {  Manifest.permission.CAMERA  }, 10);
                    }
                    dialog.dismiss();
                }
            });
            alertDialog.show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 10){
            if(grantResults[0] == -1){
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle("Peringatan");
                alertDialog.setMessage("Anda tidak dapat mengambil gambar karena permission camera ditolak. Masuk ke setting aplikasi, pilih Nami, dan aktifkan Camera permission");
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    }
                });
                alertDialog.show();
            }
        }else if(grantResults[0] == 0){
            mScannerView.startCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private static class CustomViewFinderView extends ViewFinderView {
        public static final String TRADE_MARK_TEXT = "ZXing";
        public static final int TRADE_MARK_TEXT_SIZE_SP = 40;
        public final Paint PAINT = new Paint();

        public CustomViewFinderView(Context context) {
            super(context);
            init();
        }

        public CustomViewFinderView(Context context, AttributeSet attrs) {
            super(context, attrs);
            init();
        }

        private void init() {
            PAINT.setColor(Color.WHITE);
            PAINT.setAntiAlias(true);
            float textPixelSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
                    TRADE_MARK_TEXT_SIZE_SP, getResources().getDisplayMetrics());
            PAINT.setTextSize(textPixelSize);
            setSquareViewFinder(true);
        }

        @Override
        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            drawTradeMark(canvas);
        }

        private void drawTradeMark(Canvas canvas) {
            Rect framingRect = getFramingRect();
            float tradeMarkTop;
            float tradeMarkLeft;
            if (framingRect != null) {
                tradeMarkTop = framingRect.bottom + PAINT.getTextSize() + 10;
                tradeMarkLeft = framingRect.left;
            } else {
                tradeMarkTop = 10;
                tradeMarkLeft = canvas.getHeight() - PAINT.getTextSize() - 10;
            }
            canvas.drawText(TRADE_MARK_TEXT, tradeMarkLeft, tradeMarkTop, PAINT);
        }
    }
}
