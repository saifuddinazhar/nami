package com.telkomsel.nami.universal;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.telkomsel.nami.ossosk.OssOskSurveyActivity;
import com.telkomsel.nami.R;
import com.telkomsel.nami.sekolah.SekolahCanteenListActivity;
import com.telkomsel.nami.sekolah.SekolahListViewActivity;
import com.telkomsel.nami.sekolah.SekolahMarketShareActivity;
import com.telkomsel.nami.sekolah.SekolahMarketShareByQuickCountActivity;
import com.telkomsel.nami.sekolah.SekolahMerchandisingActivity;
import com.telkomsel.nami.sekolah.SekolahProfileActivity;
import com.telkomsel.nami.universal.arrayadapter.ImageTextArrayAdapter;
import com.telkomsel.nami.universal.core.Event;
import com.telkomsel.nami.universal.core.MockLocationDetector;
import com.telkomsel.nami.universal.core.OssOsk;
import com.telkomsel.nami.universal.core.Place;
import com.telkomsel.nami.universal.core.Sekolah;
import com.telkomsel.nami.universal.core.StaticVariable;
import com.telkomsel.nami.universal.core.User;
import com.telkomsel.nami.universal.ui.BaseActivity;

public class PlaceAktivitasActivity extends BaseActivity {
    private Place place;
    private User.IUserLocationListener locationListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_aktivitas);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        place = (Place) StaticVariable.getAndRemove("selectedPlace");

        if(place instanceof OssOsk){
            getSupportActionBar().setTitle("Aktivitas OSS/OSK");
        }else if(place instanceof Sekolah){
            getSupportActionBar().setTitle("Aktivitas Sekolah");
        } else if(place instanceof Event){
            getSupportActionBar().setTitle("Aktivitas Event");
        }
        
        ListView listView = (ListView)findViewById(R.id.activity_aktivitas_oss_osk_listview);

        ImageTextArrayAdapter.ImageTextItem [] items = null;
        if(place instanceof OssOsk){
            items = new ImageTextArrayAdapter.ImageTextItem[]{
                new ImageTextArrayAdapter.ImageTextItem(R.drawable.survey, "Survey"),
                new ImageTextArrayAdapter.ImageTextItem(R.drawable.barcode, "Scan barcode"),
            };
        }else if(place instanceof Sekolah){
            items = new ImageTextArrayAdapter.ImageTextItem[]{
                new ImageTextArrayAdapter.ImageTextItem(R.drawable.school_profile, "Profile Sekolah"),
                new ImageTextArrayAdapter.ImageTextItem(R.drawable.shop, "Profile Kantin"),
                new ImageTextArrayAdapter.ImageTextItem(R.drawable.market_share, "Profile Market Share"),
                new ImageTextArrayAdapter.ImageTextItem(R.drawable.hand, "Market Share by Quick Count"),
                new ImageTextArrayAdapter.ImageTextItem(R.drawable.barcode, "Sales"),
                new ImageTextArrayAdapter.ImageTextItem(R.drawable.merchandising, "Merchandising")
            };
        }else if(place instanceof Event){
            items = new ImageTextArrayAdapter.ImageTextItem[]{
                new ImageTextArrayAdapter.ImageTextItem(R.drawable.barcode, "Sales"),
            };
        }

        ImageTextArrayAdapter adapter = new ImageTextArrayAdapter(this, items);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(place instanceof OssOsk){
                    if(i == 0){
                        Intent intent = new Intent(PlaceAktivitasActivity.this, OssOskSurveyActivity.class);
                        startActivity(intent);
                    }else if(i == 1){
                        Intent intent = new Intent(PlaceAktivitasActivity.this, BarcodePaketActivity.class);
                        intent.putExtra("channelType", "OSS_OSK");
                        startActivity(intent);
                    }
                }else if(place instanceof Sekolah){
                    if(i == 0){
                        Intent intent = new Intent(PlaceAktivitasActivity.this, SekolahProfileActivity.class);
                        startActivity(intent);
                    }else if(i == 1) {
                        Intent intent = new Intent(PlaceAktivitasActivity.this, SekolahCanteenListActivity.class);
                        startActivity(intent);
                    }else if(i == 2){
                        Intent intent = new Intent(PlaceAktivitasActivity.this, SekolahMarketShareActivity.class);
                        startActivity(intent);
                    }else if(i == 3){
                        Intent intent = new Intent(PlaceAktivitasActivity.this, SekolahMarketShareByQuickCountActivity.class);
                        startActivity(intent);
                    }else if(i == 4){
                        Intent intent = new Intent(PlaceAktivitasActivity.this, SekolahListViewActivity.class);
                        intent.putExtra("property", "sales_channel");
                        startActivity(intent);
                    }else if(i == 5){
                        Intent intent = new Intent(PlaceAktivitasActivity.this, SekolahMerchandisingActivity.class);
                        startActivity(intent);
                    }
                }else if(place instanceof Event){
                    if(i==0){
                        Intent intent = new Intent(PlaceAktivitasActivity.this, BarcodePaketActivity.class);
                        intent.putExtra("channelType", "EVENT");
                        startActivity(intent);
                    }
                }
            }
        });

        locationListener = new User.IUserLocationListener() {
            @Override
            public void onStateChange(State state) {

            }

            @Override
            public void onLocationChanged(Location location) {
                if(MockLocationDetector.isLocationFromMockProvider(PlaceAktivitasActivity.this, location)){
                    Toast.makeText(PlaceAktivitasActivity.this, "Maaf, mock location terdeteksi. Matikan semua aplikasi yang menggunakan mock location, disable mock location setting, kemudian restart device", Toast.LENGTH_SHORT);
                    finish();
                    return;
                }

                final ProgressDialog progressDialog = new ProgressDialog(PlaceAktivitasActivity.this);
                progressDialog.setMessage("Mohon tunggu, sedang mengupload data ke server");
                progressDialog.setCancelable(false);
                progressDialog.show();

                User.instance.checkOutPlace(location, new User.ICheckoutResult() {
                    @Override
                    public void onComplete(boolean isSuccess) {
                        progressDialog.dismiss();

                        if(isSuccess){
                            setResult(10);
                            finish();
                        }else {
                            Toast.makeText(PlaceAktivitasActivity.this, "Terjadi kesalahan", Toast.LENGTH_SHORT).show();
                        }
                        User.instance.removeLocationUpdate(locationListener);
                    }
                });
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
    }

    @Override
    public boolean onSupportNavigateUp() {
        setResult(10);
        finish();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.menu_profile_action_checkout){
            User.instance.requestLocationUpdates(PlaceAktivitasActivity.this, locationListener);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }
}
