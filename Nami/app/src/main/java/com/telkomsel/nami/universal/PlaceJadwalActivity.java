package com.telkomsel.nami.universal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.arrayadapter.ImageTextArrayAdapter;
import com.telkomsel.nami.universal.core.Place;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StaticVariable;
import com.telkomsel.nami.universal.core.User;

import org.json.JSONException;
import org.json.JSONObject;

public class PlaceJadwalActivity extends AppCompatActivity {
    private ListView listJadwal;
    private Place.PlaceType placeType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_jadwal);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        placeType = (Place.PlaceType)StaticVariable.getAndRemove("placeType");

        if(placeType == Place.PlaceType.OSS_OSK){
            getSupportActionBar().setTitle("Kunjungan OSS/OSK");
        }else if(placeType == Place.PlaceType.SEKOLAH){
            getSupportActionBar().setTitle("Kunjungan Sekolah");
        }

        listJadwal = findViewById(R.id.jadwal_ossosk_list);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return  true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(User.instance.getCachedCheckinPlace() != null){
            finish();
        }else {
            final ProgressDialog progressDialog = new ProgressDialog(PlaceJadwalActivity.this);
            progressDialog.setMessage("Cek jadwal OSS/OSK");
            progressDialog.setCancelable(false);
            progressDialog.show();

            String locationType = null;
            if(placeType == Place.PlaceType.OSS_OSK){
                locationType = "OSS_OSK";
            }else if(placeType == Place.PlaceType.SEKOLAH){
                locationType = "INSTITUTION";
            }

            ServerHandler.Parameter [] parameters =  { new ServerHandler.Parameter("userid", User.instance.getPhoneOrUserId()),  new ServerHandler.Parameter("locationType", locationType)  };
            ServerHandler serverHandler = new ServerHandler("sp_user_check_pjp_activities.php", parameters);
            serverHandler.addListener(new ServerHandler.Listener() {
                @Override
                public void onSuccess(String result) {
                    progressDialog.dismiss();
                    if(result.equals("false")){
                        ImageTextArrayAdapter.ImageTextItem [] items = {
                                new ImageTextArrayAdapter.ImageTextItem(R.drawable.terjadwal, "PJP Terjadwal (0/0)"),
                                new ImageTextArrayAdapter.ImageTextItem(R.drawable.tidak_terjadwal, "PJP Tidak terjadwal")
                        };
                        ImageTextArrayAdapter adapter = new ImageTextArrayAdapter(PlaceJadwalActivity.this, items);
                        listJadwal.setAdapter(adapter);
                        listJadwal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                if(i==0){
                                    Toast.makeText(PlaceJadwalActivity.this, "Anda tidak memiliki jadwal kunjungan hari ini", Toast.LENGTH_SHORT).show();
                                }else{
                                    loadActivityTidakTerjadwal();
                                }
                            }
                        });
                    }else{
                        try{
                            final JSONObject jsonResult = new JSONObject(result);
                            final boolean isTidakTerjadwalAlowed = jsonResult.getInt("allowed_non_pjp") == 0? false : true;
                            final int jumlahPjp = jsonResult.getInt("pjp");
                            final int kunjunganPjp = jsonResult.getInt("visit_pjp");

                            ImageTextArrayAdapter.ImageTextItem [] items = {
                                    new ImageTextArrayAdapter.ImageTextItem(R.drawable.terjadwal, "PJP Terjadwal (" + kunjunganPjp + "/" + jumlahPjp + ")"),
                                    new ImageTextArrayAdapter.ImageTextItem(R.drawable.tidak_terjadwal, isTidakTerjadwalAlowed ? "PJP Tidak terjadwal" : "PJP Tidak terjadwal (LOCK)"),
                            };
                            ImageTextArrayAdapter adapter = new ImageTextArrayAdapter(PlaceJadwalActivity.this, items);
                            listJadwal.setAdapter(adapter);
                            listJadwal.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                    if(i==0){
                                        Intent intent = new Intent(PlaceJadwalActivity.this, PlaceListActivity.class);
                                        intent.putExtra("command", PlaceListActivity.COMMAND_PRIORITY);
                                        StaticVariable.add("placeType", placeType);
                                        startActivity(intent);
                                    }else if(i==1){
                                        if(isTidakTerjadwalAlowed){
                                            loadActivityTidakTerjadwal();
                                        }else {
                                            AlertDialog.Builder builder = new AlertDialog.Builder(PlaceJadwalActivity.this);
                                            Toast.makeText(PlaceJadwalActivity.this, "PJP Terjadwal belum Anda selesaikan, lakukan kunjungan ke PJP Terjadwal terlebih dahulu", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            });
                        }catch (JSONException e){
                            Log.e("Nami", e.toString());
                        }
                    }
                }

                @Override
                public void onError(Exception error) {
                    error.printStackTrace();
                }
            });
            serverHandler.execute();
        }
    }

    private void loadActivityTidakTerjadwal(){
        Intent intent = new Intent(PlaceJadwalActivity.this, PlaceListActivity.class);
        intent.putExtra("command", PlaceListActivity.COMMAND_NON_PRIORITY);
        StaticVariable.add("placeType", placeType);
        startActivity(intent);
    }
}
