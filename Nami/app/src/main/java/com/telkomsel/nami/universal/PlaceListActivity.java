package com.telkomsel.nami.universal;

import android.app.ProgressDialog;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.arrayadapter.PlaceArrayAdapter;
import com.telkomsel.nami.universal.core.Configuration;
import com.telkomsel.nami.universal.core.MockLocationDetector;
import com.telkomsel.nami.universal.ui.BaseActivity;
import com.telkomsel.nami.universal.core.Event;
import com.telkomsel.nami.universal.core.OssOsk;
import com.telkomsel.nami.universal.core.Place;
import com.telkomsel.nami.universal.core.Sekolah;
import com.telkomsel.nami.universal.core.ServerHandler;
import com.telkomsel.nami.universal.core.StartupConfig;
import com.telkomsel.nami.universal.core.StaticVariable;
import com.telkomsel.nami.universal.core.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PlaceListActivity extends BaseActivity{
    public static final String COMMAND_PRIORITY ="ossosk_priority";
    public static final String COMMAND_NON_PRIORITY ="ossosk_non_priority";
    public static final String COMMAND_EVENT = "event";

    private Place.PlaceType placeType;
    private String command = null;
    private ListView listView;
    private TextView textViewNoSchedule;
    private Place [] places;
    private User.IUserLocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        placeType = (Place.PlaceType) StaticVariable.getAndRemove("placeType");

        command = getIntent().getExtras().getString("command", null);

        if(command.equals(COMMAND_PRIORITY)){
            getSupportActionBar().setTitle("PJP Terjadwal");
        }else if(command.equals(COMMAND_NON_PRIORITY)){
            getSupportActionBar().setTitle("PJP Tidak Terjadwal");
        }else if(command.equals(COMMAND_EVENT)){
            getSupportActionBar().setTitle("Event");
        }

        textViewNoSchedule = findViewById(R.id.activity_place_list_textview_noschedule);

        listView = findViewById(R.id.activity_place_list_listview_place);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Place clickedPlace = places[i];
                StaticVariable.add(PlaceProfileActivity.PROP_SELECTED_PLACE, clickedPlace);
                Intent intent = new Intent(PlaceListActivity.this, PlaceProfileActivity.class);
                startActivity(intent);
            }
        });

        locationListener = new User.IUserLocationListener() {
            @Override
            public void onStateChange(State state) {

            }

            @Override
            public void onLocationChanged(Location location) {
                if(MockLocationDetector.isLocationFromMockProvider(PlaceListActivity.this, location)){
                    Toast.makeText(PlaceListActivity.this, "Maaf, mock location terdeteksi. Matikan semua aplikasi yang menggunakan mock location, disable mock location setting, kemudian restart device", Toast.LENGTH_SHORT);
                    finish();
                    return;
                }

                double latitude = location.getLatitude();
                double longitude = location.getLongitude();

                String serverCommand = null;

                if(placeType == Place.PlaceType.OSS_OSK){
                    if(command.equals(COMMAND_PRIORITY)){
                        serverCommand = "sp_user_pjp_oss_osk_priority.php";
                    }else if (command.equals(COMMAND_NON_PRIORITY)){
                        serverCommand = "sp_user_pjp_oss_osk_non_priority.php";
                    }
                }else if(placeType == Place.PlaceType.SEKOLAH){
                    if(command.equals(COMMAND_PRIORITY)){
                        serverCommand = "sp_user_pjp_institution_priority.php";
                    }else if (command.equals(COMMAND_NON_PRIORITY)){
                        serverCommand = "sp_user_pjp_institution_non_priority.php";
                    }
                }else if(placeType == Place.PlaceType.EVENT){
                    serverCommand = "sp_event_location_read.php";
                }

                final ProgressDialog progressDialog = new ProgressDialog(PlaceListActivity.this);
                progressDialog.setMessage("Mohon tunggu, sedang mendapatkan informasi jadwal dari server");
                progressDialog.setCancelable(false);
                progressDialog.show();

                ServerHandler.Parameter [] parameters =  {
                        new ServerHandler.Parameter("userid", User.instance.getPhoneOrUserId()),
                        new ServerHandler.Parameter("latitude", Double.toString(latitude)),
                        new ServerHandler.Parameter("longitude", Double.toString(longitude))
                };
                ServerHandler serverHandler = new ServerHandler(serverCommand, parameters);
                serverHandler.addListener(new ServerHandler.Listener() {
                    @Override
                    public void onSuccess(String result) {
                        progressDialog.dismiss();

                        try {
                            if(result.equals("false")){
                                onNoSchedule();
                            }else{
                                JSONArray jsonPlaces = new JSONArray(result);
                                places = new Place[jsonPlaces.length()];
                                if(jsonPlaces.length() > 0){
                                    for(int i=0; i<jsonPlaces.length(); i++){
                                        JSONObject jsonPlace = jsonPlaces.getJSONObject(i);
                                        Place place = null;
                                        if(command.equals(COMMAND_PRIORITY) || command.equals(COMMAND_NON_PRIORITY)){
                                            if(placeType == Place.PlaceType.OSS_OSK){
                                                place = new OssOsk(jsonPlace);
                                            }else if(placeType == Place.PlaceType.SEKOLAH){
                                                place = new Sekolah(jsonPlace);
                                            }
                                        }else if(command.equals(COMMAND_EVENT)){
                                            place = new Event(jsonPlace);
                                        }
                                        places[i] = place;
                                    }
                                }else{
                                    onNoSchedule();
                                }

                                PlaceArrayAdapter adapter = new PlaceArrayAdapter(PlaceListActivity.this, places);
                                listView.setAdapter(adapter);
                            }
                        } catch (JSONException e) {
                            Log.e("Nami", e.toString());
                        }
                    }

                    @Override
                    public void onError(Exception error) {
                        Log.e("Nami", error.toString());
                    }
                });
                serverHandler.execute();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {
            }
        };
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return  true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        if(User.instance.getCachedCheckinPlace() != null){
            finish();
        }

        User.instance.requestLocationUpdateWithCheckPermission(this, locationListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        User.instance.removeLocationUpdate(locationListener);
    }


    private void onNoSchedule(){
        listView.setVisibility(View.GONE);
        textViewNoSchedule.setVisibility(View.VISIBLE);
    }
}
