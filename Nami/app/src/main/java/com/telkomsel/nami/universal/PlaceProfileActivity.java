package com.telkomsel.nami.universal;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Pair;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.arrayadapter.PlaceProfileArrayAdapter;
import com.telkomsel.nami.universal.core.Configuration;
import com.telkomsel.nami.universal.core.Event;
import com.telkomsel.nami.universal.core.MockLocationDetector;
import com.telkomsel.nami.universal.core.OssOsk;
import com.telkomsel.nami.universal.core.Place;
import com.telkomsel.nami.universal.core.Sekolah;
import com.telkomsel.nami.universal.core.StaticVariable;
import com.telkomsel.nami.universal.core.User;
import com.telkomsel.nami.ossosk.OssOskCloseReasonActivity;
import com.telkomsel.nami.universal.ui.BaseActivity;

import java.text.SimpleDateFormat;

public class PlaceProfileActivity extends BaseActivity {
    public static String PROP_SELECTED_PLACE = "selected_oss_osk";

    private Place selectedPlace;
    private ProgressDialog progressDialog;
    private State state;

    private User.IUserLocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        selectedPlace = (Place) StaticVariable.getAndRemove(PROP_SELECTED_PLACE);

        final LinearLayout linearLayoutBottom = (LinearLayout)findViewById(R.id.activity_profile_oss_osk_linearlayout_bottom);
        ListView listView = (ListView)findViewById(R.id.activity_profile_oss_osk_listview_place);
        final Button buttonCheckIn = (Button)findViewById(R.id.activity_profile_oss_osk_button_checkin);
        Button buttonClose = null;

        if(selectedPlace == null){
            Place checkedInPlace = (Place)User.instance.getCachedCheckinPlace();
            selectedPlace = checkedInPlace;
            buttonCheckIn.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            buttonCheckIn.setText("Next");
            state = State.NEXT;
        } else {
            if(selectedPlace.getIsInRangeToUser()){
                LinearLayout.LayoutParams layoutParams  = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.weight = 0.5f;

                if(selectedPlace instanceof OssOsk){
                    buttonClose = new Button(this);
                    buttonClose.setLayoutParams(layoutParams);
                    buttonClose.setText("Tutup");
                    buttonClose.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    buttonClose.setTextColor(getResources().getColor(R.color.white));
                    linearLayoutBottom.addView(buttonClose, 0);

                    final Place finalSelectedOssOsk1 = selectedPlace;
                    buttonClose.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            StaticVariable.add("selectedOssOsk", finalSelectedOssOsk1);
                            Intent intent = new Intent(PlaceProfileActivity.this, OssOskCloseReasonActivity.class);
                            startActivityForResult(intent, 0);
                        }
                    });
                }

                buttonCheckIn.setLayoutParams(layoutParams);
                buttonCheckIn.setBackgroundColor(getResources().getColor(R.color.darkGreen));
                buttonCheckIn.setText("Check In");
                state = State.CHECK_IN;
            }else {
                buttonCheckIn.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                buttonCheckIn.setText("Out Of Range");
                state = State.OUT_OF_RANGE;
            }
        }

        if(selectedPlace instanceof OssOsk){
            getSupportActionBar().setTitle("Profile OSS/OSK");
        }else if(selectedPlace instanceof Sekolah){
            getSupportActionBar().setTitle("Profile Sekolah");
        }else if(selectedPlace instanceof Event){
            getSupportActionBar().setTitle("Profile Event");
        }

        final Place finalSelectedPlace = selectedPlace;
        final Button finalButtonClose = buttonClose;
        buttonCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (state){
                    case CHECK_IN:
                        progressDialog = new ProgressDialog(PlaceProfileActivity.this);
                        progressDialog.setMessage("Checkin..");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        User.instance.checkInPlace(finalSelectedPlace, new User.ICheckinResult() {
                            @Override
                            public void onComplete(boolean isSuccess) {
                                progressDialog.hide();
                                if(isSuccess){
                                    if(finalButtonClose != null) linearLayoutBottom.removeView(finalButtonClose);

                                    buttonCheckIn.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                    buttonCheckIn.setText("Next");
                                    state = State.NEXT;
                                    invalidateOptionsMenu();

                                    Intent intent = new Intent(PlaceProfileActivity.this, PlaceAktivitasActivity.class);
                                    StaticVariable.add("selectedPlace", selectedPlace);
                                    startActivityForResult(intent,10);
                                }else {
                                    Toast.makeText(PlaceProfileActivity.this, "Terjadi kesalahan, kontak administrator untuk info lebih lanjut", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        break;
                    case NEXT:
                        Intent intent = new Intent(PlaceProfileActivity.this, PlaceAktivitasActivity.class);
                        StaticVariable.add("selectedPlace", selectedPlace);
                        startActivityForResult(intent,10);
                        break;
                    case OUT_OF_RANGE:
                        Toast.makeText(PlaceProfileActivity.this, "Lokasi OSS/OSK terlalu jauh dari lokasi Anda, dekati OSS/OSK untuk melakukan check-in", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        PlaceProfileArrayAdapter.Content [] pairs = null;
        final PlaceProfileArrayAdapter.Content [] distancePair = new PlaceProfileArrayAdapter.Content[1];

        if(selectedPlace instanceof OssOsk || selectedPlace instanceof Sekolah){
            pairs = new PlaceProfileArrayAdapter.Content[10];
            pairs[0] =  new PlaceProfileArrayAdapter.Content("Id", selectedPlace.getId());
            pairs[1] =  new PlaceProfileArrayAdapter.Content("Nama", selectedPlace.getNama());
            pairs[2] =  new PlaceProfileArrayAdapter.Content("Alamat", selectedPlace.getAddress());
            pairs[3] =  new PlaceProfileArrayAdapter.Content("Regional", selectedPlace.getRegional());
            pairs[4] =  new PlaceProfileArrayAdapter.Content("Branch", selectedPlace.getBranch());
            pairs[5] =  new PlaceProfileArrayAdapter.Content("Cluster", selectedPlace.getCluster());
            pairs[6] =  new PlaceProfileArrayAdapter.Content("Kota", selectedPlace.getCity());
            pairs[7] =  new PlaceProfileArrayAdapter.Content("Latitude", Double.toString(selectedPlace.getLocation().getLatitude()));
            pairs[8] =  new PlaceProfileArrayAdapter.Content("Longitude", Double.toString(selectedPlace.getLocation().getLongitude()));
            pairs[9] =  new PlaceProfileArrayAdapter.Content("Jarak", String.format("%.02f", selectedPlace.getDistanceToUser()) + " m");
            distancePair[0] = pairs[9];
        }else if(selectedPlace instanceof Event){
            Event event = (Event) selectedPlace;
            pairs = new PlaceProfileArrayAdapter.Content[8];
            pairs[0] =  new PlaceProfileArrayAdapter.Content("Id", event.getId());
            pairs[1] =  new PlaceProfileArrayAdapter.Content("Nama", event.getNama());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            pairs[2] =  new PlaceProfileArrayAdapter.Content("Tanggal Mulai", (dateFormat.format(event.getStartDate())));
            pairs[3] =  new PlaceProfileArrayAdapter.Content("Tanggal Selesai", (dateFormat.format(event.getStartDate())));
            pairs[4] =  new PlaceProfileArrayAdapter.Content("Surrounding POI", event.getSurroundingPOI());
            pairs[5] =  new PlaceProfileArrayAdapter.Content("Latitude", Double.toString(event.getLocation().getLatitude()));
            pairs[6] =  new PlaceProfileArrayAdapter.Content("Longitude", Double.toString(event.getLocation().getLongitude()));
            pairs[7] =  new PlaceProfileArrayAdapter.Content("Jarak", String.format("%.02f", event.getDistanceToUser()) + " m");
            distancePair[0] =  pairs[7];
        }

        PlaceProfileArrayAdapter placeProfileArrayAdapter = new PlaceProfileArrayAdapter(this, pairs);
        listView.setAdapter(placeProfileArrayAdapter);

        locationListener = new User.IUserLocationListener() {
            @Override
            public void onStateChange(State state) {

            }

            @Override
            public void onLocationChanged(Location location) {
                if(MockLocationDetector.isLocationFromMockProvider(PlaceProfileActivity.this, location)){
                    Toast.makeText(PlaceProfileActivity.this, "Maaf, mock location terdeteksi. Matikan semua aplikasi yang menggunakan mock location, disable mock location setting, kemudian restart device", Toast.LENGTH_SHORT);
                    finish();
                    return;
                }
                distancePair[0].setValue(String.format("%.02f", selectedPlace.getDistanceToUser()) + " m");
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();
        User.instance.requestLocationUpdateWithCheckPermission(this, locationListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        User.instance.removeLocationUpdate(locationListener);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 0){
            if(resultCode == Activity.RESULT_OK){
                finish();
            }
        }else if(requestCode == 10){
            if(User.instance.getCachedCheckinPlace() == null){
                finish();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private enum State { CHECK_IN, NEXT, OUT_OF_RANGE}
}
