package com.telkomsel.nami.universal.arrayadapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.telkomsel.nami.R;

/**
 * Created by saifu on 1/15/2018.
 */

public class ImageTextArrayAdapter extends ArrayAdapter {
    private Activity context;
    private ImageTextItem[] imageTextItems;

    public ImageTextArrayAdapter(@NonNull Activity context, @NonNull ImageTextItem[] imageTextItems) {
        super(context, R.layout.list_item_image_text, imageTextItems);
        this.context = context;
        this.imageTextItems = imageTextItems;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_item_image_text, null,true);

        ImageView image = (ImageView) rowView.findViewById(R.id.list_item_image);
        TextView text = (TextView) rowView.findViewById(R.id.list_item_text);

        image.setImageResource(imageTextItems[position].getImageId());
        text.setText(imageTextItems[position].getText());

        return  rowView;
    }

    public static class  ImageTextItem {
        private int imageId;
        private String text;

        public ImageTextItem(int imageId, String text){
            this.imageId = imageId;
            this.text = text;
        }

        public int getImageId(){
            return  imageId;
        }

        public String getText(){
            return text;
        }
    }
}
