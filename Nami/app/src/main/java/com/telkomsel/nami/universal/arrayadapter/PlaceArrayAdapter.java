package com.telkomsel.nami.universal.arrayadapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.telkomsel.nami.R;
import com.telkomsel.nami.universal.core.Place;

/**
 * Created by saifu on 1/19/2018.
 */

public class PlaceArrayAdapter extends ArrayAdapter {
    private Activity context;
    private Place [] places;

    public PlaceArrayAdapter(@NonNull Activity context, Place[] places) {
        super(context, R.layout.list_item_place, places);
        this.context = context;
        this.places = places;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_item_place, null,true);

        TextView textViewName = (TextView) rowView.findViewById(R.id.place_list_item_textview_name);
        TextView textViewDistance = (TextView) rowView.findViewById(R.id.place_list_item_textview_distance);
        TextView textViewInRange = (TextView) rowView.findViewById(R.id.place_list_item_textview_inrange);

        textViewName.setText(places[position].getNama());
        textViewDistance.setText("Jarak : " + String.format("%.02f", places[position].getDistanceToUser()) + " m");
        if(places[position].getIsInRangeToUser()){
            textViewInRange.setText("In range");
            textViewInRange.setTextColor(Color.parseColor("#098700"));
        }else {
            textViewInRange.setText("Out of range");
            textViewInRange.setTextColor(Color.RED);
        }

        return  rowView;
    }
}
