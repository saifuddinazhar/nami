package com.telkomsel.nami.universal.arrayadapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.telkomsel.nami.R;

/**
 * Created by saifu on 1/20/2018.
 */

public class PlaceProfileArrayAdapter extends ArrayAdapter {
    private Activity context;
    private Content [] pairs;

    public PlaceProfileArrayAdapter(@NonNull Activity context, Content [] pairs) {
        super(context, R.layout.list_item_place_profile, pairs);
        this.context = context;
        this.pairs = pairs;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_item_place_profile, null,true);

        final TextView textViewTitle = (TextView)rowView.findViewById(R.id.place_profile_list_item_title);
        final TextView textViewValue = (TextView)rowView.findViewById(R.id.place_profile_list_item_value);

        textViewTitle.setText(pairs[position].getKey());
        textViewValue.setText(pairs[position].getValue());

        pairs[position].onChangeListener = new Content.IOnChangeListener() {

            @Override
            public void onKeyChange(Content content, String key) {

            }

            @Override
            public void onValueChange(Content content, String value) {
                textViewValue.setText(pairs[position].getValue());
            }
        };

        return  rowView;
    }

    public static class Content {
        private String key, value;
        private IOnChangeListener onChangeListener;

        public Content(String key, String value){
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }

        public void setKey(String key) {
            this.key = key;
            if(onChangeListener != null) onChangeListener.onValueChange(this, this.key);
        }

        public void setValue(String value) {
            this.value = value;
            if(onChangeListener != null) onChangeListener.onValueChange(this, this.value);
        }

        public interface IOnChangeListener {
            void onKeyChange(Content content, String key);
            void onValueChange(Content content, String value);
        }
    }
}
