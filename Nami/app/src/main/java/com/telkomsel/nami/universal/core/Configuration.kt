package com.telkomsel.nami.universal.core

object Configuration {
    val IS_DEBUG_MODE  = false;
    val SERVER_URL: String
     get() {
         if(IS_DEBUG_MODE)
             return "http://163.53.193.92/nami2018dev/";
         else
             return "http://163.53.193.92/nami2018/";
     }
}
