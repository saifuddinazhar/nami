package com.telkomsel.nami.universal.core;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by saifu on 3/6/2018.
 */

public class Event extends Place {
    public Event(JSONObject jsonPlace) {
        super(jsonPlace);
    }

    @Override
    public String getNama() {
        try {
            return jsonPlace.getString("event_name");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String getId() {
        try {
            return jsonPlace.getString("id_event");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean getIsInRangeToUser() {
        return getDistanceToUser() <= StartupConfig.getRadiusEvent() ? true : false;
    }

    public Date getStartDate(){
        try {
            String dateString = jsonPlace.getString("start_date");

            try {
                DateFormat df = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.ENGLISH);
                return  df.parse(dateString);
            } catch (ParseException e) {
                try {
                    DateFormat df = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
                    return  df.parse(dateString);
                } catch (ParseException e1) {
                    Log.e("Nami", e1.toString());
                    return  null;
                }
            }
        } catch (JSONException e) {
            Log.e("Nami", e.toString());
            return null;
        }
    }

    public Date getEndDate(){
        try {
            String dateString = jsonPlace.getString("end_date");

            try {
                DateFormat df = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.ENGLISH);
                return  df.parse(dateString);
            } catch (ParseException e) {
                try {
                    DateFormat df = new SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH);
                    return  df.parse(dateString);
                } catch (ParseException e1) {
                    Log.e("Nami", e1.toString());
                    return  null;
                }
            }
        } catch (JSONException e) {
            Log.e("Nami", e.toString());
            return null;
        }
    }

    public String getSurroundingPOI(){
        try {
            return jsonPlace.getString("surrounding_poi");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
