package com.telkomsel.nami.universal.core;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by saifu on 1/18/2018.
 */

public class OssOsk extends Place {
    public OssOsk(JSONObject jsonPlace) {
        super(jsonPlace);
    }

    @Override
    public String getNama() {
        try {
            return jsonPlace.getString("outlet_name");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String getId() {
        return getIdOutletSefiia();
    }

    @Override
    public boolean getIsInRangeToUser() {
        return getDistanceToUser() <= StartupConfig.getRadiusOssOsk() ? true : false;
    }

    public String getIdOutletSefiia(){
        return getValueFromJSONObject(jsonPlace,"id_outlet_sefiia");
    }
}

