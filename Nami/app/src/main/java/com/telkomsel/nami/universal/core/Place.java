package com.telkomsel.nami.universal.core;

import android.location.Location;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by saifu on 1/18/2018.
 */

public abstract class Place {
    protected JSONObject jsonPlace;

    public Place(JSONObject jsonPlace){
        this.jsonPlace = jsonPlace;
    }

    public JSONObject getJSONPlace(){
        return jsonPlace;
    }

    public abstract String getNama();

    public abstract String getId();

    public Location getLocation(){
        try {
            double latitude = jsonPlace.getDouble("latitude");
            double longitude = jsonPlace.getDouble("longitude");

            Location location = new Location("");
            location.setLatitude(latitude);
            location.setLongitude(longitude);
            return location;
        } catch (JSONException e) {
            return null;
        }
    }

    public float getDistanceToUser(){
        Location userLocation = User.instance.getCachedLocation();
        if(userLocation != null){
            return userLocation.distanceTo(getLocation());
        }else{
            return  -1;
        }
    }

    protected String getValueFromJSONObject(JSONObject jsonObject, String value){
        try {
            return jsonObject.getString(value);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getArea(){
        return getValueFromJSONObject(jsonPlace,"area");
    }

    public String getRegional(){
        return getValueFromJSONObject(jsonPlace,"regional");
    }

    public String getBranch(){
        return getValueFromJSONObject(jsonPlace,"branch");
    }

    public String getCluster(){
        return getValueFromJSONObject(jsonPlace,"cluster");
    }

    public String getCity(){
        return getValueFromJSONObject(jsonPlace,"city");
    }

    public String getAddress(){
        return getValueFromJSONObject(jsonPlace,"address");
    }

    public String getDistance(){
        return getValueFromJSONObject(jsonPlace,"distance");
    }

    public abstract boolean getIsInRangeToUser();

    public enum PlaceType {OSS_OSK, SEKOLAH, EVENT}
}
