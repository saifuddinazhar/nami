package com.telkomsel.nami.universal.core;

import android.graphics.Bitmap;
import android.util.Base64;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

/**
 * Created by saifu on 2/7/2018.
 */

public class Sekolah extends Place {
    public Sekolah(JSONObject jsonPlace) {
        super(jsonPlace);
    }

    @Override
    public String getNama() {
        try {
            return jsonPlace.getString("institution_name");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String getId() {
        try {
            return jsonPlace.getString("npsn");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean getIsInRangeToUser() {
        return getDistanceToUser() <= StartupConfig.getRadiusSekolah() ? true : false;
    }

    public void getCanteens(final OnGetCanteenObtained onGetCanteenObtained){
        ServerHandler.Parameter [] parameters =  { new ServerHandler.Parameter("npsn", User.instance.getCachedCheckinPlace().getId())};
        ServerHandler serverHandler = new ServerHandler("sp_institution_canteen_location_read.php", parameters);
        serverHandler.addListener(new ServerHandler.Listener() {
            @Override
            public void onSuccess(String result) {
                try{
                    JSONArray jsonCanteens = new JSONArray(result);

                    ArrayList<Canteen> canteens = new ArrayList<>();
                    if(jsonCanteens != null && jsonCanteens.length() > 0){
                        for(int i=0; i<jsonCanteens.length(); i++){
                            Canteen canteen = new Canteen(jsonCanteens.getJSONObject(i), Sekolah.this);
                            canteens.add(canteen);
                        }
                    }
                    onGetCanteenObtained.onSuccess(canteens.toArray(new Canteen[canteens.size()]));
                } catch (JSONException exception) {
                    onError(exception);
                }
            }


            @Override
            public void onError(Exception error) {
                onGetCanteenObtained.onError(error);
            }
        });
        serverHandler.execute();
    }

    public void AddCanteen(String canteenName, String ownerName, String ownerContact, String mKios, final OnAddCanteenResult onAddCanteenResult){
        ServerHandler.Parameter [] parameters = new ServerHandler.Parameter[]{
                new ServerHandler.Parameter("npsn", getId()),
                new ServerHandler.Parameter("canteenName", canteenName),
                new ServerHandler.Parameter("ownerName", ownerName),
                new ServerHandler.Parameter("ownerContact", ownerContact),
                new ServerHandler.Parameter("mKios", mKios == null || mKios.equals("") ? "" : mKios),
                new ServerHandler.Parameter("userId", User.instance.getPhoneOrUserId()),
                new ServerHandler.Parameter("username", User.instance.getUsername()),
                new ServerHandler.Parameter("modes", "insert")
        };
        ServerHandler serverHandler = new ServerHandler("sp_institution_canteen_location_insert.php", parameters);
        serverHandler.addListener(new ServerHandler.Listener() {
            @Override
            public void onSuccess(String result) {
                if(result.equals("true")){
                    onAddCanteenResult.onSuccess();
                }else{
                    onAddCanteenResult.onError(new Exception("DuplicateCanteenName"));
                }
            }

            @Override
            public void onError(Exception error) {
                onAddCanteenResult.onError(error);
            }
        });
        serverHandler.execute();
    }

    public static class Canteen {
        private Sekolah sekolah;
        private JSONObject jsonCanteen;

        public Canteen(JSONObject jsonCanteen, Sekolah sekolah){
            this.jsonCanteen = jsonCanteen;
            this.sekolah = sekolah;
        }

        public String getId(){
            try {
                return jsonCanteen.getString("id_canteen");
            } catch (JSONException e) {
                e.printStackTrace();
                return  null;
            }
        }

        public String getName(){
            try {
                return jsonCanteen.getString("canteen_name");
            } catch (JSONException e) {
                e.printStackTrace();
                return  null;
            }
        }

        public String getOwnerName(){
            try {
                return jsonCanteen.getString("owner_name");
            } catch (JSONException e) {
                e.printStackTrace();
                return  null;
            }
        }

        public String getOwnerContact(){
            try {
                return jsonCanteen.getString("owner_contact");
            } catch (JSONException e) {
                e.printStackTrace();
                return  null;
            }
        }

        public String getMKios(){
            try {
                return jsonCanteen.getString("rs_mkios");
            } catch (JSONException e) {
                e.printStackTrace();
                return  null;
            }
        }

        public void update(String canteenName, String ownerName, String ownerContact, String mKios,
                           final String shopSignBranding, final String tableClothBranding, final int sachet,
                           final Bitmap image, final OnUpdateResult onUpdateResult){

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            image.compress(Bitmap.CompressFormat.JPEG, 90, stream); //compress to which format you want.
            byte [] imageBytes = stream.toByteArray();
            final String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

            ServerHandler.Parameter [] parameters = new ServerHandler.Parameter[]{
                    new ServerHandler.Parameter("npsn", sekolah.getId()),
                    new ServerHandler.Parameter("canteenName", canteenName),
                    new ServerHandler.Parameter("ownerName", ownerName),
                    new ServerHandler.Parameter("ownerContact", ownerContact),
                    new ServerHandler.Parameter("mKios",  mKios == null || mKios.equals("") ? "" : mKios),
                    new ServerHandler.Parameter("userId", User.instance.getPhoneOrUserId()),
                    new ServerHandler.Parameter("username", User.instance.getUsername()),
                    new ServerHandler.Parameter("modes", "update")
            };

            ServerHandler serverHandler = new ServerHandler("sp_institution_canteen_location_insert.php", parameters);
            serverHandler.addListener(new ServerHandler.Listener() {
                @Override
                public void onSuccess(String result) {
                    if(result.equals("true")){
                        ServerHandler.Parameter [] parameters2 = new ServerHandler.Parameter[]{
                                new ServerHandler.Parameter("npsn", sekolah.getId()),
                                new ServerHandler.Parameter("idcanteen", sekolah.getId() + "_" + getName().toUpperCase()),
                                new ServerHandler.Parameter("shopsignbranding", shopSignBranding),
                                new ServerHandler.Parameter("tableclothbranding", tableClothBranding),
                                new ServerHandler.Parameter("sachet", Integer.toString(sachet)),
                                new ServerHandler.Parameter("imageData", imageString),
                                new ServerHandler.Parameter("userid", User.instance.getPhoneOrUserId()),
                                new ServerHandler.Parameter("username", User.instance.getUsername())
                        };
                        ServerHandler serverHandler2 = new ServerHandler("sp_institution_canteen_insert.php", parameters2);
                        serverHandler2.addListener(new ServerHandler.Listener() {
                            @Override
                            public void onSuccess(String result) {
                                if(result.equals("true")){
                                    onUpdateResult.onSuccess();
                                }else {
                                    onError(new Exception(result));
                                }
                            }

                            @Override
                            public void onError(Exception error) {
                                onUpdateResult.onError(error);
                            }
                        });
                        serverHandler2.execute();
                    }else{
                        onUpdateResult.onError(new Exception(result));
                    }
                }

                @Override
                public void onError(Exception error) {
                    onUpdateResult.onError(error);
                }
            });
            serverHandler.execute();
        }

        public interface OnUpdateResult{
            void onSuccess();
            void onError(Exception error);
        }
    }

    public interface OnGetCanteenObtained {
        void onSuccess(Canteen [] canteens);
        void onError(Exception error);
    }

    public interface OnAddCanteenResult{
        void onSuccess();
        void onError(Exception error);
    }
}
