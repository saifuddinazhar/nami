package com.telkomsel.nami.universal.core;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class ServerHandler extends AsyncTask {
    private String serverUrl = Configuration.INSTANCE.getSERVER_URL();
    private String commandPage = "", urlParameter = "";
    private ArrayList<Listener> listeners = new ArrayList<>();

    public ServerHandler(String commandPage, Parameter [] parameters){
        this.commandPage = commandPage;
        try {
            if(parameters.length > 0){
                for(int i=0; i<parameters.length; i++){
                    Parameter parameter = parameters[i];

                    if(i==0){
                        urlParameter += parameter.getName() + "=" + URLEncoder.encode(parameter.getValue(), "UTF-8");
                    }else {
                        urlParameter += "&" + parameter.getName() + "=" + URLEncoder.encode(parameter.getValue(), "UTF-8");
                    }
                }
            }
        } catch (UnsupportedEncodingException e) {
            for (Listener listener:listeners) {
                listener.onError(e);
            }
        }
    }

    public void addListener(Listener listener){
        listeners.add(listener);
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        HttpURLConnection connection = null;
        try {
            try {
                Log.d("Nami", "Send query to server, command : " + commandPage + ", urlParam : " + urlParameter);

                URL url = new URL(serverUrl + commandPage);
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setConnectTimeout(5000);
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connection.setRequestProperty("Content-Length", Integer.toString(urlParameter.getBytes().length).toString());
                connection.setRequestProperty("Content-Language", "en-US");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                //Send request
                DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
                wr.writeBytes(urlParameter);
                wr.flush();
                wr.close();

                //Get Response
                InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));
                String line;
                StringBuffer response = new StringBuffer();
                while ((line = rd.readLine()) != null) {
                    response.append(line);
                }
                rd.close();
                return response.toString();
            } catch (java.net.SocketTimeoutException e) {
                return e;
            } catch (java.io.IOException e) {
                return e;
            }
        } catch (Exception e) {
            return e;
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        if(o instanceof Exception){
            for (Listener listener:listeners) {
                listener.onError((Exception)o);
            }
        }else if(o instanceof String){
            for (Listener listener:listeners) {
                Log.d("Nami", "Result query from server, command : " + commandPage + ", urlParam : " + urlParameter + ", result : " + o);
                listener.onSuccess((String)o);
            }
        }else{
            for (Listener listener:listeners) {
                listener.onError(new Exception("UnknownError"));
            }
        }
    }

    public static class Parameter{
        private String name;
        private String value;

        public Parameter(String name, String value){
            this.name = name;
            this.value = value;
        }

        public String getName(){
            return name;
        }

        public String getValue(){
            return value;
        }
    }

    public interface Listener{
        void onSuccess(String result);
        void onError(Exception error);
    }
}
