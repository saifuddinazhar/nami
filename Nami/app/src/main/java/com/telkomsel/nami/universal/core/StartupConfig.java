package com.telkomsel.nami.universal.core;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by saifu on 3/22/2018.
 */

public class StartupConfig {
    private static HashMap<String, String> startupConfigs = new HashMap<>();

    public static void initialize(final IInitializeListener listener){
        ServerHandler.Parameter [] parameters = new ServerHandler.Parameter[0];
        ServerHandler serverHandler = new ServerHandler("sp_config_startup.php", parameters);
        serverHandler.addListener(new ServerHandler.Listener() {
            @Override
            public void onSuccess(String result) {
                try{
                    JSONArray jsonStartupConfigs = new JSONArray(result);
                    if(jsonStartupConfigs.length() > 0){
                        for(int i=0; i<jsonStartupConfigs.length(); i++){
                            JSONObject jsonStartupConfig = jsonStartupConfigs.getJSONObject(i);
                            startupConfigs.put(jsonStartupConfig.getString("param_variable"), jsonStartupConfig.getString("param_value"));
                        }
                    }
                    listener.onSuccess();
                }catch (JSONException exception){
                    onError(exception);
                }
            }

            @Override
            public void onError(Exception error) {
                listener.onError(error);
            }
        });
        serverHandler.execute();
    }

    public static String getAppVersion(){
        return startupConfigs.get("app_version");
    }

    public static int getSekolahCanteenSachet(){
        return Integer.parseInt(startupConfigs.get("institution_canteen_sachet"));
    }

    public static int getSekolahMarketShareAmt(){
        return Integer.parseInt(startupConfigs.get("institution_market_share_isiulang_amt"));
    }

    public static int getSekolahMarketShareTrx(){
        return Integer.parseInt(startupConfigs.get("institution_market_share_isiulang_trx"));
    }

    public static int getSekolahMarketShareQuickCountAmt(){
        return Integer.parseInt(startupConfigs.get("institution_market_share_quickcount_amt"));
    }

    public static int getSekolahMerchandisingBanner(){
        return Integer.parseInt(startupConfigs.get("institution_merchandising_banner"));
    }

    public static int getSekolahMerchandisingPoster(){
        return Integer.parseInt(startupConfigs.get("institution_merchandising_poster"));
    }

    public static int getSekolahMerchandisingShopsign(){
        return Integer.parseInt(startupConfigs.get("institution_merchandising_shopsign"));
    }

    public static int getSekolahMerchandisingTableCloth(){
        return Integer.parseInt(startupConfigs.get("institution_merchandising_tablecloth"));
    }

    public static int getOssOskSurveyDisplay(){
        return Integer.parseInt(startupConfigs.get("oss_osk_survey_diplay"));
    }

    public static int getOssOskSurveyProdukTerlakuQuota(){
        return Integer.parseInt(startupConfigs.get("oss_osk_survey_produk_terlaku_kuota"));
    }

    public static int getOssOskSurveyProdukTerlakuTp(){
        return Integer.parseInt(startupConfigs.get("oss_osk_survey_produk_terlaku_tp"));
    }

    public static int getOssOskSurveyRecharge(){
        return Integer.parseInt(startupConfigs.get("oss_osk_survey_recharge"));
    }

    public static int getOssOskSurveySalesBroadband(){
        return Integer.parseInt(startupConfigs.get("oss_osk_survey_sales_broadband"));
    }

    public static String [] getReligions(){
        String stringReligions = startupConfigs.get("religions");
        String [] religions = stringReligions.split(",");
        return religions;
    }

    public static int getRadiusOssOsk(){
        return Integer.parseInt(startupConfigs.get("radius_oss_osk_inmeters"));
    }

    public static int getRadiusSekolah(){
        return Integer.parseInt(startupConfigs.get("radius_institution_inmeters"));
    }

    public static int getRadiusEvent(){
        return Integer.parseInt(startupConfigs.get("radius_event_inmeters"));
    }

    public interface IInitializeListener{
        void onSuccess();
        void onError(Exception error);
    }
}
