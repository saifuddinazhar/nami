package com.telkomsel.nami.universal.core;

import java.util.HashMap;

/**
 * Created by saifu on 1/19/2018.
 */

public class StaticVariable {
    private static HashMap<String, Object> hashMap = new HashMap<>();

    public static boolean add(String key, Object value){
        if(hashMap.containsKey(key)){
            return false;
        }
        hashMap.put(key, value);
        return true;
    }


    public static Object get(String key, Object defaultValue){
        if(hashMap.containsKey(key)){
            return hashMap.get(key);
        }
        return defaultValue;
    }

    public static Object get(String key){
        return get(key, null);
    }

    public static Object getAndRemove(String key, Object defaultValue){
        Object object = get(key, defaultValue);
        remove(key);
        return object;
    }

    public static Object getAndRemove(String key){
        return getAndRemove(key, null);
    }


    public static boolean remove(String key){
        if(hashMap.containsKey(key)){
            hashMap.remove(key);
            return true;
        }
        return false;
    }
}
