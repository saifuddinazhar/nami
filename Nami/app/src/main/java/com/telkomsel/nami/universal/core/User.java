package com.telkomsel.nami.universal.core;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.telkomsel.nami.universal.PlaceListActivity;
import com.telkomsel.nami.universal.ui.BaseActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by saifu on 1/15/2018.
 */

public class User {
    public static User instance;

    private JSONObject jsonUser;
    private Place checkedInPlace;
    private LocationManager locationManager;

    public User(JSONObject jsonUser){
        this.jsonUser = jsonUser;
    }

    public String getPhoneOrUserId(){
        try {
            return jsonUser.getString("user_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getUsername(){
        try {
            return  jsonUser.getString("user_name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  null;
    }

    public String getUserType(){
        try {
            return  jsonUser.getString("user_type");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  null;
    }

    public String getBranch(){
        try {
            return  jsonUser.getString("branch");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  null;
    }

    public Location getCachedLocation(){
        if(locationManager == null) return null;
        try{
            Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location == null) {
                location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            }
            return location;
        } catch (SecurityException e){
            return null;
        }
    }

    public void  removeLocationUpdate(LocationListener listener){
        if(locationManager != null){
            locationManager.removeUpdates(listener);
        }
    }

    public void requestLocationUpdates(final BaseActivity activity, final IUserLocationListener listener){
        final ProgressDialog progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage("Mohon tunggu, sedang mendapatkan informasi lokasi");
        progressDialog.setCancelable(false);
        progressDialog.show();

        try{
            if(locationManager == null) locationManager = (LocationManager)activity.getSystemService(LOCATION_SERVICE);

            if(locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                //Try GPS_PROVIDER
                final Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                if(location != null){
                    progressDialog.dismiss();
                    listener.onLocationChanged(location);
                }
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, listener);

                //Count in a few second if fail using NETWORK_PROVIDER
                new CountDownTimer(6000, 1000) {
                    @Override
                    public void onTick(long l) { }

                    @Override
                    public void onFinish() {
                        if(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) == null){
                            progressDialog.dismiss();
                            locationManager.removeUpdates(listener);
                            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 10, listener);
                        }
                    }
                }.start();
            } else {
                progressDialog.dismiss();
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 10, listener);
            }
        }catch (SecurityException e){
            Toast.makeText(activity, "Error on request location update : " + e.toString(), Toast.LENGTH_SHORT).show();
            Log.e("Nami", e.toString());
        }
    }

    public void requestLocationUpdateWithCheckPermission(final BaseActivity activity, final IUserLocationListener listener){
        if ( ContextCompat.checkSelfPermission( activity, android.Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED ) {
            requestLocationUpdates(activity, listener);
        }else{
            AlertDialog alertDialog = new AlertDialog.Builder(activity).create();
            alertDialog.setTitle("Peringatan");
            alertDialog.setMessage("Permission GPS diperlukan untuk mendapatkan data lokasi Anda");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions( activity, new String[] {  Manifest.permission.ACCESS_FINE_LOCATION , Manifest.permission.ACCESS_COARSE_LOCATION }, 10); ;
                    dialog.dismiss();
                }
            });
            alertDialog.show();

            activity.getOnRequestPermissionResultListeners().add(new BaseActivity.IOnRequestPermissionResultListener() {
                @Override
                public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
                if(requestCode == 10){
                    if(grantResults[0] == -1){
                        Toast.makeText(activity, "Anda tidak dapat masuk menu ini karena GPS permission ditolak. Masuk ke setting aplikasi, pilih Nami, dan aktifkan GPS permission", Toast.LENGTH_SHORT).show();
                        activity.finish();
                    } else if(grantResults[0] == 0){
                        requestLocationUpdates(activity, listener);
                    }
                }

                activity.getOnRequestPermissionResultListeners().remove(this);
                }
            });
        }
    }

    public void checkInWithReason(final Place place, String reason, final ICheckinResult iCheckinResult){
        String locationType = null;

        if(place instanceof OssOsk){
            locationType = "OSS_OSK";
        }else if(place instanceof Sekolah){
            locationType = "INSTITUTION";
        }else if(place instanceof Event){
            locationType = "EVENT";
        }

        ServerHandler.Parameter [] parameters =  {
                new ServerHandler.Parameter("locationid", place.getId()),
                new ServerHandler.Parameter("locationname", place.getNama()),
                new ServerHandler.Parameter("userid", User.instance.getPhoneOrUserId()),
                new ServerHandler.Parameter("username", User.instance.getUsername()),
                new ServerHandler.Parameter("latitude", Double.toString(User.instance.getCachedLocation().getLatitude())),
                new ServerHandler.Parameter("longitude", Double.toString(User.instance.getCachedLocation().getLongitude())),
                new ServerHandler.Parameter("locationtype", locationType),
                new ServerHandler.Parameter("reason", reason),
        };
        ServerHandler serverHandler = new ServerHandler("sp_user_checkin_insert.php", parameters);
        serverHandler.addListener(new ServerHandler.Listener() {
            @Override
            public void onSuccess(String result) {
                if(result.equals("correct")){
                    checkedInPlace = place;
                    iCheckinResult.onComplete(true);
                }else {
                    iCheckinResult.onComplete(false);
                }
            }

            @Override
            public void onError(Exception error) {
                error.printStackTrace();
                iCheckinResult.onComplete(false);
            }
        });
        serverHandler.execute();
    }

    public void checkInPlace(final Place place, final ICheckinResult iCheckinResult){
        checkInWithReason(place, "-", iCheckinResult);
    }

    public void checkInClosePlace(final Place place,  String reason, final ICheckoutResult iCheckoutResult){
        checkInWithReason(place, reason, new ICheckinResult() {
            @Override
            public void onComplete(boolean isSuccess) {
            if(isSuccess){
                checkOutPlace(getCachedLocation(), iCheckoutResult);
            }else {
                iCheckoutResult.onComplete(false);
            }
            }
        });
    }

    public void getCheckedInPlace(final ICheckedInPlaceResult iCheckedInPlaceResult){
        if(checkedInPlace == null){
            ServerHandler.Parameter [] parameters =  {
                    new ServerHandler.Parameter("userid", getPhoneOrUserId())
            };
            ServerHandler serverHandler = new ServerHandler("sp_user_checkout_status.php", parameters);
            serverHandler.addListener(new ServerHandler.Listener() {
                @Override
                public void onSuccess(String result) {
                    if(result.equals("false")){
                        iCheckedInPlaceResult.onResult(null);
                    }else {
                        try {
                            JSONObject jsonPlace = (new JSONObject(result)).getJSONObject("place");
                            if(jsonPlace.getString("location_type").equals("OSS_OSK")){
                                OssOsk ossOsk = new OssOsk(jsonPlace);
                                checkedInPlace = ossOsk;
                                iCheckedInPlaceResult.onResult(ossOsk);
                            }else if(jsonPlace.getString("location_type").equals("INSTITUTION")){
                                Sekolah sekolah = new Sekolah(jsonPlace);
                                checkedInPlace = sekolah;
                                iCheckedInPlaceResult.onResult(sekolah);
                            }else if(jsonPlace.getString("location_type").equals("EVENT")) {
                                Event event = new Event(jsonPlace);
                                checkedInPlace = event;
                                iCheckedInPlaceResult.onResult(event);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            iCheckedInPlaceResult.onResult(null);
                        }
                    }
                }

                @Override
                public void onError(Exception error) {
                    error.printStackTrace();
                    iCheckedInPlaceResult.onResult(null);
                }
            });
            serverHandler.execute();
        }else {
            iCheckedInPlaceResult.onResult(checkedInPlace);
        }
    }

    public Place getCachedCheckinPlace(){
        return checkedInPlace;
    }

    public void checkOutPlace(Location checkoutLocation, final ICheckoutResult iCheckoutResult){
        ServerHandler.Parameter [] parameters =  {
                new ServerHandler.Parameter("userid", getPhoneOrUserId()),
                new ServerHandler.Parameter("latitude", Double.toString(checkoutLocation.getLatitude())),
                new ServerHandler.Parameter("longitude", Double.toString(checkoutLocation.getLongitude())),
        };
        ServerHandler serverHandler = new ServerHandler("sp_user_checkout_insert.php", parameters);
        serverHandler.addListener(new ServerHandler.Listener() {
            @Override
            public void onSuccess(String result) {
                Log.e("Nami", result);

                if(result.equals("correct")){
                    checkedInPlace = null;
                    iCheckoutResult.onComplete(true);
                }else {
                    iCheckoutResult.onComplete(false);
                }
            }

            @Override
            public void onError(Exception error) {
                error.printStackTrace();
                iCheckoutResult.onComplete(false);
            }
        });
        serverHandler.execute();
    }

    public interface ICheckinResult{
        void onComplete(boolean isSuccess);
    }

    public interface ICheckoutResult{
        void onComplete(boolean isSuccess);
    }

    public interface ICheckedInPlaceResult{
        void onResult(Place place);
    }

    public static interface IUserLocationListener extends LocationListener{
        void onStateChange(State state);

        enum State { ERROR_ON_GPS_PERMISSION_REJECTED};

    }
}
