package com.telkomsel.nami.universal.core;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import java.util.List;

/**
 * Created by saifu on 2/20/2018.
 */

public class Utility {
    public static String convertOperatorIdToString(int id){
        switch (id){
            case 0 : return "TELKOMSEL";
            case 1 : return "OOREDO";
            case 2 : return "XL";
            case 3 : return "AXIS";
            case 4 : return "3";
            case 5 : return "OTHER";
        }
        return null;
    }

    public static String convertDateToIndonesianString(int d, int m, int y){
        return d + "/" + m + "/" + y;
    }

    public static String convertDateIndonesianStringToMysqlString(String indonesianDateString){
        try{
            String [] splittedDate = indonesianDateString.split("/");
            String mysqlDateString = splittedDate[2] + "-" + splittedDate[1] + "-" + splittedDate[0];
            return mysqlDateString;
        }catch (Exception e){
            Log.d("Nami", e.toString());
            return null;
        }
    }

    public static String convertDateMySqlStringToIndonesianString(String mysqlString){
        try{
            String [] splittedDate = mysqlString.split("-");
            String indonesianString = splittedDate[2] + "/" + splittedDate[1] + "/" + splittedDate[0];
            return indonesianString;
        }catch (Exception e){
            Log.d("Nami", e.toString());
            return null;
        }
    }

    public static String convertPhoneNumberToStandardLocalPhoneNmber(String number){

        if(number.length() < 11 || number.length() > 14) return null;

        boolean isValid = false;

        if(number.charAt(0) == '0'){
            number = number.substring(1, number.length());
            number = "62" + number;
            isValid = true;
        } else if(number.charAt(0) == '8'){
            number = "62" + number;
            isValid = true;
        } else if(number.substring(0, 2).equals("62")){
            isValid = true;
        }

        if(isValid) return  number;

        return null;
    }

    private static String [] telkomselValidationPrefixs = { "62811","62812","62813","62821","62822","62823","62852","62853","62851"};
    private static String [] ooredooValidationPrefixs = {"62814","62815","62816","62855","62856","62857","62858"};
    private static String [] xlValidationPrefixs = {"62817","62818","62819","62859","62877","62878"};
    private static String [] axisValidationPrefixs = {"62838","62831","62832","62833"};
    private static String [] _3ValidationPrefixs = {"62895","62896","62897","62898","62899"};

    public static Operator detectOperatorFromPhoneNumber(String number){
        number = convertPhoneNumberToStandardLocalPhoneNmber(number);

        if(number == null || number.length() < 5){
            return Operator.INVALID_NUMBER;
        }

        String prefix = number.substring(0, 5);

        for (String eachPrefix:telkomselValidationPrefixs) {
            if(prefix.equals(eachPrefix)){
                return Operator.TELKOMSEL;
            }
        }

        for (String eachPrefix:ooredooValidationPrefixs) {
            if(prefix.equals(eachPrefix)){
                return Operator.OOREDOO;
            }
        }

        for (String eachPrefix:xlValidationPrefixs) {
            if(prefix.equals(eachPrefix)){
                return Operator.XL;
            }
        }

        for (String eachPrefix:axisValidationPrefixs) {
            if(prefix.equals(eachPrefix)){
                return Operator.AXIS;
            }
        }

        for (String eachPrefix:_3ValidationPrefixs) {
            if(prefix.equals(eachPrefix)){
                return Operator._3;
            }
        }

        return Operator.OTHER;
    }

    public static boolean isLocationHighAccuracyEnabled(Context context){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                int locationMode = Settings.Secure.getInt(context.getContentResolver(),Settings.Secure.LOCATION_MODE);
                return (locationMode != Settings.Secure.LOCATION_MODE_OFF && locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY);
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return  false;
            }
        }else{
            String locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public enum Operator {INVALID_NUMBER, TELKOMSEL, OOREDOO, XL, AXIS, _3, OTHER}
}
