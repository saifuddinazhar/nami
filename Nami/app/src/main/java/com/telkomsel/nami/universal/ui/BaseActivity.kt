package com.telkomsel.nami.universal.ui

import android.support.v7.app.AppCompatActivity
import java.util.ArrayList

abstract class BaseActivity : AppCompatActivity(){
    val onRequestPermissionResultListeners = ArrayList<IOnRequestPermissionResultListener>()

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        for (listener in onRequestPermissionResultListeners){
            listener.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    interface IOnRequestPermissionResultListener {
        fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray);
    }
}