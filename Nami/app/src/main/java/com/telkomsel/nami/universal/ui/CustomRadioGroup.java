package com.telkomsel.nami.universal.ui;

import android.view.View;
import android.widget.RadioButton;

import java.util.ArrayList;

/**
 * Created by saifu on 2/20/2018.
 */

public class CustomRadioGroup implements View.OnClickListener {
    private OnItemSelectedListener onItemSelectedListener;
    private ArrayList<RadioButton> radioButtons = new ArrayList<>();

    public void add(RadioButton radioButton){
        radioButtons.add(radioButton);
        radioButton.setOnClickListener(this);
    }

    public RadioButton [] getRadioButtons(){
        return  radioButtons.toArray(new RadioButton[radioButtons.size()]);
    }

    public void setOnItemSelectedListener(OnItemSelectedListener onItemSelectedListener){
        this.onItemSelectedListener = onItemSelectedListener;
    }

    @Override
    public void onClick(View view) {
        if(radioButtons.size() > 0){
            for(int i=0; i<radioButtons.size(); i++){
                RadioButton radioButton = radioButtons.get(i);

                if(view.getId() == radioButton.getId()){
                    radioButton.setChecked(true);
                    if(onItemSelectedListener != null) onItemSelectedListener.onSelect(this, i);
                } else {
                    radioButton.setChecked(false);
                }
            }
        }
    }

    public RadioButton getSelectedItem(){
        for (RadioButton radioButton:radioButtons) {
            if(radioButton.isChecked()) return  radioButton;
        }
        return null;
    }

    public int getSelectedItemIdx(){
        RadioButton selectedRadioButton = getSelectedItem();
        if(selectedRadioButton != null){
            for(int i=0; i<radioButtons.size(); i++){
                RadioButton radioButton = radioButtons.get(i);
                if(radioButton.getId() == selectedRadioButton.getId()) return i;
            }
        }
        return -1;
    }

    public interface OnItemSelectedListener{
        void onSelect(CustomRadioGroup radioGroup, int idx);
    }
}
