<?php
include "connectdb.php";

if( isset($_POST['npsn']) && 
	isset($_POST['principalname']) && isset($_POST['principalcontact']) && isset($_POST['principalbirthday']) && isset($_POST['principalhobby']) && isset($_POST['principalreligion']) && 
	isset($_POST['influencername']) && isset($_POST['influencertitle']) && isset($_POST['influencercontact']) && isset($_POST['influencerbirthday']) && isset($_POST['influencerhobby']) && isset($_POST['influencerreligion']) && 
	isset($_POST['chairmanname']) && isset($_POST['chairmancontact']) && isset($_POST['chairmanbirthday']) && isset($_POST['chairmanhobby']) && isset($_POST['chairmanreligion']) && 
	isset($_POST['ambassadorname']) && isset($_POST['ambassadorcontact']) && isset($_POST['ambassadorbirthday']) && isset($_POST['ambassadorhobby']) && isset($_POST['ambassadorreligion']) &&
	isset($_POST['ambassadorfb']) && isset($_POST['ambassadorinstagram']) && isset($_POST['ambassadorline'])	&& isset($_POST['totalambassador']) &&
	isset($_POST['institutionbirthday']) && isset($_POST['interestedactivities']) && isset($_POST['institutionfb']) && isset($_POST['institutioninstagram']) && isset($_POST['institutionurl']) && isset($_POST['userid']) && isset($_POST['username']) &&
	isset($_POST['totalteacher']) && isset($_POST['totalstudents']) && isset($_POST['totallaboratorium']) && isset($_POST['totalsportroom']) && isset($_POST['totalclassroom']) && isset($_POST['totalcooperative']) && isset($_POST['totalcanteen']) && 
	isset($_POST['networktelkomsel']) && isset($_POST['networkooredoo']) && isset($_POST['networkxl']) && isset($_POST['networkaxis']) && isset($_POST['network3']) && isset($_POST['networkothers']) && 
	isset($_POST['fieldbranding']) && isset($_POST['wallbranding']) && isset($_POST['wallmagazinebranding']) && isset($_POST['modes'])){
	
	$npsn = $_POST['npsn'];
	$principalname = $_POST['principalname']; $principalcontact = $_POST['principalcontact']; $principalbirthday = $_POST['principalbirthday']; $principalhobby = $_POST['principalhobby']; $principalreligion = $_POST['principalreligion'];
	$influencername = $_POST['influencername']; $influencertitle = $_POST['influencertitle']; $influencercontact = $_POST['influencercontact']; $influencerbirthday = $_POST['influencerbirthday']; $influencerhobby = $_POST['influencerhobby']; $influencerreligion = $_POST['influencerreligion']; 
	$chairmanname = $_POST['chairmanname']; $chairmancontact = $_POST['chairmancontact']; $chairmanbirthday = $_POST['chairmanbirthday']; $chairmanhobby = $_POST['chairmanhobby']; $chairmanreligion = $_POST['chairmanreligion']; 
	$ambassadorname = $_POST['ambassadorname']; $ambassadorcontact = $_POST['ambassadorcontact']; $ambassadorbirthday = $_POST['ambassadorbirthday']; $ambassadorhobby = $_POST['ambassadorhobby']; $ambassadorreligion = $_POST['ambassadorreligion'];
	$ambassadorfb = $_POST['ambassadorfb']; $ambassadorinstagram = $_POST['ambassadorinstagram']; $ambassadorline = $_POST['ambassadorline']; $totalambassador = $_POST['totalambassador'];
	$institutionbirthday = $_POST['institutionbirthday']; $interestedactivities = $_POST['interestedactivities']; $institutionfb = $_POST['institutionfb'];  $institutioninstagram = $_POST['institutioninstagram'];  $institutionurl = $_POST['institutionurl']; $userid = $_POST['userid']; $username = $_POST['username'];
	$totalteacher = $_POST['totalteacher']; $totalstudents = $_POST['totalstudents']; $totallaboratorium = $_POST['totallaboratorium']; $totalsportroom = $_POST['totalsportroom']; $totalclassroom = $_POST['totalclassroom']; $totalcooperative = $_POST['totalcooperative']; $totalcanteen = $_POST['totalcanteen']; 
	$networktelkomsel = $_POST['networktelkomsel']; $networkooredoo = $_POST['networkooredoo']; $networkxl = $_POST['networkxl']; $networkaxis = $_POST['networkaxis']; $network3 = $_POST['network3']; $networkothers = $_POST['networkothers']; 
	$fieldbranding = $_POST['fieldbranding']; $wallbranding = $_POST['wallbranding']; $wallmagazinebranding = $_POST['wallmagazinebranding']; $modes = $_POST['modes']; 

	$sql = "CALL sp_institution_profiling_insert('{$npsn}', 
		'{$principalname}', '{$principalcontact}', '{$principalbirthday}', '{$principalhobby}', '{$principalreligion}', 
		'{$influencername}', '{$influencertitle}', '{$influencercontact}', '{$influencerbirthday}', '{$influencerhobby}', '{$influencerreligion}', 
		'{$chairmanname}', '{$chairmancontact}', '{$chairmanbirthday}', '{$chairmanhobby}', '{$chairmanreligion}', 
		'{$ambassadorname}', '{$ambassadorcontact}', '{$ambassadorbirthday}', '{$ambassadorhobby}', '{$ambassadorreligion}',
		'{$ambassadorfb}', '{$ambassadorinstagram}', '{$ambassadorline}', '{$totalambassador}', 
		'{$institutionbirthday}', '{$interestedactivities}', '{$institutionfb}', '{$institutioninstagram}', '{$institutionurl}', {$userid}, '{$username}', 
		{$totalteacher}, {$totalstudents}, {$totallaboratorium}, {$totalsportroom}, {$totalclassroom}, {$totalcooperative}, {$totalcanteen}, 
		'{$networktelkomsel}', '{$networkooredoo}', '{$networkxl}', '{$networkaxis}', '{$network3}', '{$networkothers}', 
		'{$fieldbranding}', '{$wallbranding}', '{$wallmagazinebranding}', NOW(), '{$modes}'
		)";
		
	$result = $conn->query($sql);
	
	if($result->num_rows > 0){
		foreach ($result as $key) {
			echo "true";
			break;
		}
	}else{
		echo "false";
	}
}else{
	echo "ccfalse";
}
?>