<?php
include "connectdb.php";

$isComplete = true;

if(!(isset($_POST['salesTelkomsel']) && isset($_POST['salesOoredoo']) && isset($_POST['salesXl'])
		&& isset($_POST['salesAxis']) && isset($_POST['sales3']) && isset($_POST['salesOther']))){
		
	echo "Error : Sales is not complete";
	$isComplete = false;
}

if(!(isset($_POST['displayTelkomsel']) && isset($_POST['displayOoredoo']) && isset($_POST['displayXl'])
		&& isset($_POST['displayAxis']) && isset($_POST['display3']) && isset($_POST['displayOther']))){
		
	echo "Error : Display is not complete";
	$isComplete = false;
}

if(!(isset($_POST['rechargeTelkomsel']) && isset($_POST['rechargeOoredoo']) && isset($_POST['rechargeXl'])
		&& isset($_POST['rechargeAxis']) && isset($_POST['recharge3']) && isset($_POST['rechargeOther']))){
		
	echo "Error : Recharge is not complete";
	$isComplete = false;
}

if(!($_POST['networkTelkomsel'] && $_POST['networkOoredoo'] && $_POST['networkXl']
		&& $_POST['networkAxis'] && $_POST['network3'] && $_POST['networkOther'])){
		
	echo "Error : Network is not complete";
	$isComplete = false;
}

if(!(isset($_POST['quotaTelkomsel']) && isset($_POST['tpTelkomsel']) && isset($_POST['eupTelkomsel'])
		&& isset($_POST['quotaOoredoo']) && isset($_POST['tpOoredoo']) && isset($_POST['eupOoredoo'])
		&& isset($_POST['quotaXl']) && isset($_POST['tpXl']) && isset($_POST['eupXl'])
		&& isset($_POST['quotaAxis']) && isset($_POST['tpAxis']) && isset($_POST['eupAxis'])
		&& isset($_POST['quota3']) && isset($_POST['tp3']) && isset($_POST['eup3'])
		&& isset($_POST['quotaOther']) && isset($_POST['tpOther']) && isset($_POST['eupOther']))){
		
	echo "Error : Produk terlaku is not complete";
	$isComplete = false;
}

if(!($_POST['dominantBranding'] && $_POST['imageData'])){
		
	echo "Error : Branding is not complete";
	$isComplete = false;
}

if(!($_POST['userId'] && $_POST['username'] && $_POST['idOutlet'] && $_POST['outletName'])){
		
	echo "Error : Other info is not complete";
	$isComplete = false;
}

if($isComplete){
	$imagePath = "Images/".$_POST['userId']."_".date("Ymd_His").".jpg";

	$sql = "CALL sp_oss_osk_profiling_insert(
			'".$_POST['idOutlet']."',
			'".$_POST['outletName']."',
	
			".$_POST['salesTelkomsel'].",
			".$_POST['salesOoredoo'].",
			".$_POST['salesXl'].",
			".$_POST['salesAxis'].",
			".$_POST['sales3'].",
			".$_POST['salesOther'].",	
			
			".$_POST['displayTelkomsel'].",
			".$_POST['displayOoredoo'].",
			".$_POST['displayXl'].",
			".$_POST['displayAxis'].",
			".$_POST['display3'].",
			".$_POST['displayOther'].",	
			
			".$_POST['rechargeTelkomsel'].",
			".$_POST['rechargeOoredoo'].",
			".$_POST['rechargeXl'].",
			".$_POST['rechargeAxis'].",
			".$_POST['recharge3'].",
			".$_POST['rechargeOther'].",	
			
			'".$_POST['networkTelkomsel']."',
			'".$_POST['networkOoredoo']."',
			'".$_POST['networkXl']."',
			'".$_POST['networkAxis']."',
			'".$_POST['network3']."',
			'".$_POST['networkOther']."',
			
			".$_POST['quotaTelkomsel'].",
			".$_POST['tpTelkomsel'].",
			".$_POST['eupTelkomsel'].",
			".$_POST['quotaOoredoo'].",
			".$_POST['tpOoredoo'].",
			".$_POST['eupOoredoo'].",
			".$_POST['quotaXl'].",
			".$_POST['tpXl'].",
			".$_POST['eupXl'].",
			".$_POST['quotaAxis'].",
			".$_POST['tpAxis'].",
			".$_POST['eupAxis'].",
			".$_POST['quota3'].",
			".$_POST['tp3'].",
			".$_POST['eup3'].",
			".$_POST['quotaOther'].",
			".$_POST['tpOther'].",
			".$_POST['eupOther'].",
			
			'".$_POST['dominantBranding']."',
			'".$imagePath."',
			
			'".$_POST['userId']."',
			'".$_POST['username']."',
			NOW()
		)";

	$result = $conn->query($sql);
	if($result->num_rows < 1){
		echo "false";
	} else {
		//file_put_contents($imagePath, base64_decode($_POST['imageData']));
		$binary=base64_decode($_POST['imageData']);
		$file = fopen($imagePath, "wb");
		fwrite($file, $binary);
		fclose($file);
		echo "true";
	}
}
?>